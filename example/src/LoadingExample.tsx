import React from 'react';
import { Content, Loading } from 'react-native-biza';

export default function LoadingExample() {
  return (
    <Content centerContent>
      <Loading />
    </Content>
  );
}
