import React from 'react';
import { StyleSheet } from 'react-native';
import { Content, Footer, Layer, Surface, FooterBar, Text } from 'react-native-biza';

export default function FooterExample() {
  return (
    <>
      <Content centerContent>
        <Text>Content</Text>
      </Content>
      <Footer>
        <Layer type="underlay">
          <Surface style={styles.background} />
        </Layer>
        <FooterBar>
          <Text>Footer</Text>
        </FooterBar>
      </Footer>
    </>
  );
}

const styles = StyleSheet.create({
  background: { flex: 1 },
});
