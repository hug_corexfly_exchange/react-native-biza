
import React from 'react';
import { Content, Button, useModalController } from 'react-native-biza';

export default function ModalExample() {
  const controller = useModalController();

  return (
    <Content scrollable centerContent>
      <Button
        onPress={() => {
          controller.alert('This is first alert!', { title: 'Alert', closeable: true });
        }}
      >
        Show Alert
      </Button>
    </Content>
  );
}
