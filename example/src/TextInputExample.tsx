import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import { Content, Surface, Layout, Boundary, TextInput, Button, Text } from 'react-native-biza';

export default function Example() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [verification, setVerification] = useState('');

  const invalidVerification = !(/^[0-9]*$/).test(verification);

  return (
    <Content keyboardShouldPersistTaps="handled" contentContainerStyle={styles.container}>
      <Surface>
        <Layout padding>
          <TextInput
            placeholder="아이디"
            clearButtonMode="while-has-value"
            value={username}
            onChangeText={setUsername}
            style={styles.wrapper}
          />
        </Layout>
      </Surface>
      <Surface>
        <TextInput
          editable={false}
          placeholder="비밀번호"
          value={password}
          onChangeText={setPassword}
        />
      </Surface>
      <Surface>
        <Layout type="row" padding>
          <Layout sizing={10}>
            <Boundary
              outlineRight={0}
              message={invalidVerification && '숫자만 입력 가능합니다.'}
            >
              <TextInput
                placeholder="인증번호"
                value={verification}
                onChangeText={setVerification}
              />
            </Boundary>
          </Layout>
          <Layout sizing={3}>
            <Boundary
              roundedTopLeft={0}
              roundedBottomLeft={0}
              message={invalidVerification}
            >
              <Button type="transparent">
                <Text weight="bold">발송</Text>
              </Button>
            </Boundary>
          </Layout>
        </Layout>
      </Surface>
      <Surface>
        <Layout padding>
          <Boundary>
            <Layout type="row" alignItems="center">
              <Layout sizing={1}>
                <TextInput
                  placeholder="인증번호"
                  value={verification}
                  onChangeText={setVerification}
                />
              </Layout>
              <Layout padding>
                <Text>3:00</Text>
              </Layout>
            </Layout>
          </Boundary>
        </Layout>
      </Surface>
    </Content>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
  },
  wrapper: {
    borderWidth: 1,
    borderColor: 'lightgray',
    borderRadius: 6,
  },
});
