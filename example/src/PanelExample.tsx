import React from 'react';
import { Content, Panel, Layout, Text } from 'react-native-biza';

export default function PanelExample() {
  return (
    <Content centerContent>
      <Panel>
        <Text>edge: "bottom"</Text>
      </Panel>
      <Layout marginTop>
        <Panel edge="top">
          <Text>edge: "top"</Text>
        </Panel>
      </Layout>
    </Content>
  );
}
