import React from 'react';
import { Content, Number, CoinText } from 'react-native-biza';

export default function NumberExample() {
  return (
    <Content centerContent>
      <Number value={1000} />
      <Number value={1000} format={{ style: 'currency', currency: 'KRW' }} />
      <CoinText>
        <Number value={1000} />
      </CoinText>
    </Content>
  );
};
