import React from 'react';
import { Alert } from 'react-native';
import { Content, Surface, Layout, Button } from 'react-native-biza';

export default function ButtonExample() {
  return (
    <Content centerContent>
      <Surface>
        <Layout padding type="row" wrap="wrap">
          {[
            'tiny' as const,
            'small' as const,
            'regular' as const,
            'large' as const,
          ].map((size, i) => (
            <Layout width="100%" marginBottom key={i}>
              <Button size={size} block onPress={() => Alert.alert('Size', size)}>
                {size}
              </Button>
            </Layout>
          ))}
          <Button block disabled>Disabled</Button>
        </Layout>
      </Surface>
    </Content>
  );
}
