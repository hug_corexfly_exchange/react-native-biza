import React from 'react';
import { StyleSheet } from 'react-native';
import { Content, Layout, Text } from 'react-native-biza';

export default function LayoutExample() {
  return (
    <Content centerContent>
      <Layout style={styles.box}>
        <Text>padding: false</Text>
      </Layout>
      <Layout padding marginTop style={styles.box}>
        <Text>padding: true</Text>
        <Text>marginTop: true</Text>
      </Layout>
    </Content>
  );
}

const styles = StyleSheet.create({
  box: { backgroundColor: 'white' },
});
