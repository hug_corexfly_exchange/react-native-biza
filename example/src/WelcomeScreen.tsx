import React from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import LogoSvg from './logo.svg';

export default function WelcomeScreen() {
  const navigation = useNavigation();
  const gettingStarted = () => navigation.navigate('Home');

  return (
    <View style={styles.container}>
      <View>
        <LogoSvg />
      </View>
      <View style={styles.padding}>
        <Text style={styles.welcome}>Welcome to the</Text>
        <Text style={styles.this}>
          <Text style={styles.this}>
            <Text style={styles.strong}>R</Text>
          eact
        </Text>
          <Text> </Text>
          <Text style={styles.this}>
            <Text style={styles.strong}>N</Text>
          ative
        </Text>
          <Text> </Text>
          <Text style={styles.this}>
            <Text style={styles.strong}>B</Text>
          iza!
        </Text>
        </Text>
      </View>
      <View style={styles.padding}>
        <Button title="Getting Started" onPress={gettingStarted} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F1F3F5',
  },
  padding: {
    paddingTop: 50,
  },
  welcome: {
    fontSize: 28,
    fontWeight: '500',
    textAlign: 'center',
  },
  this: {
    fontSize: 36,
    fontWeight: '300',
    textAlign: 'center',
  },
  strong: {
    fontWeight: 'bold',
  },
});
