import React from 'react';
import { Divider, Content, Layout, Surface, Text } from 'react-native-biza';

export default function DividerExample() {
  return (
    <Content contentContainerStyle={{ flex: 1, justifyContent: 'center' }}>
      <Surface>
        <Layout type="row" padding alignItems="center">
          <Text>Emma T  </Text>
          <Divider type="vertical" size={14} />
          <Text>  독일어학원</Text>
        </Layout>
      </Surface>
      <Layout marginVertical>
        <Divider color="black" />
      </Layout>
      <Layout paddingHorizontal>
        <Divider color="black" />
      </Layout>
    </Content>
  );
}
