import * as React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { ThemeProvider, PortalHost, ModalProvider, ToastProvider } from 'react-native-biza';
import { UIManager, Platform } from 'react-native';

import AvatarExample from './AvatarExample';
import BadgeExample from './BadgeExample';
import BoundaryExample from './BoundaryExample';
import ButtonExample from './ButtonExample';
import CheckboxExample from './CheckboxExample';
import CoinTextExample from './CoinTextExample';
import ContentExample from './ContentExample';
import DividerExample from './DividerExample';
import FooterExample from './FooterExample';
import HeaderExample from './HeaderExample';
import HomeScreen from './HomeScreen';
import LayerExample from './LayerExample';
import LayoutExample from './LayoutExample';
import LoadingExample from './LoadingExample';
import ModalExample from './ModalExample';
import NumberExample from './NumberExample';
import PanelExample from './PanelExample';
import PortalExample from './PortalExample';
import RadioExample from './RadioExample';
import RatingTextExample from './RatingTextExample';
import SafeAreaViewExample from './SafeAreaViewExample';
import SkeletonExample from './SkeletonExample';
import SurfaceExample from './SurfaceExample';
import TextExample from './TextExample';
import TextInputExample from './TextInputExample';
import ToastExample from './ToastExample';
import ViewExample from './ViewExample';
import WelcomeScreen from './WelcomeScreen';
import WrapperExample from './WrapperExample';

if (Platform.OS === 'android') {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

const Stack = createStackNavigator();

export default function App() {
  return (
    <SafeAreaProvider>
      <ThemeProvider>
        <ModalProvider>
          <ToastProvider>
            <PortalHost>
              <NavigationContainer>
                <Stack.Navigator initialRouteName="Welcome">
                  <Stack.Screen name="Avatar" component={AvatarExample} />
                  <Stack.Screen name="Badge" component={BadgeExample} />
                  <Stack.Screen name="Boundary" component={BoundaryExample} />
                  <Stack.Screen name="Button" component={ButtonExample} />
                  <Stack.Screen name="Checkbox" component={CheckboxExample} />
                  <Stack.Screen name="CoinText" component={CoinTextExample} />
                  <Stack.Screen name="Content" component={ContentExample} />
                  <Stack.Screen name="Divider" component={DividerExample} />
                  <Stack.Screen name="Footer" component={FooterExample} />
                  <Stack.Screen name="Header" component={HeaderExample} options={{ headerShown: false }} />
                  <Stack.Screen name="Home" component={HomeScreen} />
                  <Stack.Screen name="Layer" component={LayerExample} />
                  <Stack.Screen name="Layout" component={LayoutExample} />
                  <Stack.Screen name="Loading" component={LoadingExample} />
                  <Stack.Screen name="Modal" component={ModalExample} />
                  <Stack.Screen name="Number" component={NumberExample} />
                  <Stack.Screen name="Panel" component={PanelExample} />
                  <Stack.Screen name="Portal" component={PortalExample} />
                  <Stack.Screen name="Radio" component={RadioExample} />
                  <Stack.Screen name="RatingText" component={RatingTextExample} />
                  <Stack.Screen name="SafeAreaView" component={SafeAreaViewExample} options={{ headerShown: false }} />
                  <Stack.Screen name="Skeleton" component={SkeletonExample} />
                  <Stack.Screen name="Surface" component={SurfaceExample} />
                  <Stack.Screen name="Text" component={TextExample} />
                  <Stack.Screen name="TextInput" component={TextInputExample} />
                  <Stack.Screen name="Toast" component={ToastExample} />
                  <Stack.Screen name="View" component={ViewExample} />
                  <Stack.Screen name="Welcome" component={WelcomeScreen} options={{ headerShown: false }} />
                  <Stack.Screen name="Wrapper" component={WrapperExample} />
                </Stack.Navigator>
              </NavigationContainer>
            </PortalHost>
          </ToastProvider>
        </ModalProvider>
      </ThemeProvider>
    </SafeAreaProvider>
  );
}
