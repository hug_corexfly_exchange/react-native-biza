import React from 'react';
import { StyleSheet, Platform, TouchableOpacity } from 'react-native';
import { Content, SafeAreaView, Text } from 'react-native-biza';
import { useNavigation } from '@react-navigation/native';

export default function SafeAreaViewExample() {
  const navigaiton = useNavigation();

  return (
    <SafeAreaView inset={Platform.select({ ios: 'auto', default: 'top' })} style={styles.container}>
      <Content centerContent>
        <Text>White area is insets for safe area.</Text>
        <TouchableOpacity onPress={() => navigaiton.goBack()}>
          <Text color="primary">Back to the home</Text>
        </TouchableOpacity>
      </Content>

    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});
