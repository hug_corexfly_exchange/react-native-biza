import React from 'react';
import { Content, RatingText, Text } from 'react-native-biza';

export default function RatingTextExample() {
  return (
    <Content centerContent>
      <RatingText>4.5</RatingText>
      <RatingText size="level4">
        <Text weight="bold">4.5</Text>
        <Text size="level5">(30)</Text>
      </RatingText>
    </Content>
  );
};
