import React from 'react';
import { StyleSheet } from 'react-native';
import { Content, PortalHost, Portal, Layout, Text, Layer } from 'react-native-biza';

export default function PortalExample() {
  return (
    <Content scrollable centerContent>
      <PortalHost>
        <Layout sizing={1} alignSelf="stretch" style={styles.parent}>
          <Text>This is parent view</Text>
          <Layout style={styles.child}>
            <Text>This is child view</Text>
            <Portal>
              <Layer type="overlay">
                <Layout sizing={1} justify="center" alignItems="center">
                  <Text>Where am i?</Text>
                </Layout>
              </Layer>
            </Portal>
          </Layout>
        </Layout>
      </PortalHost>
    </Content>
  );
}

const styles = StyleSheet.create({
  parent: {
    backgroundColor: 'rgba(255, 0, 0, .5)',
  },
  child: {
    backgroundColor: 'rgba(0, 255, 0, .5)',
  },
});
