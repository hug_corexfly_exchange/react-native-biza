import React, { useState } from 'react';

import { Content , Checkbox } from 'react-native-biza';

export default function CheckboxExample() {
  const [flag, setFlag] = useState(false);
  
  return (
    <Content centerContent>
      <Checkbox checked={flag} onPress={() => setFlag(!flag)} />
    </Content>
  );
}
