import React from 'react';
import { Content, Surface, Layout, Text } from 'react-native-biza';

export default function TextExample() {
  return (
    <Content scrollable centerContent>
      <Surface>
        <Layout padding>
          <Text>Hello, world!</Text>
          <Text weight="bold">weight: "bold"</Text>
          <Text color="primary">color: "primary"</Text>
        </Layout>
      </Surface>
      <Layout margin>
        <Surface>
          <Layout padding>
            <Text weight="bold">
              <Text>inherit & </Text>
              <Text color="primary">color: "primary"</Text>
            </Text>
          </Layout>
        </Surface>
      </Layout>
    </Content>
  );
}
