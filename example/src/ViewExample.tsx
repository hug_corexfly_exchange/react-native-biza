import React, { useRef } from 'react';
import { useEffect } from 'react';
import { Animated, StyleSheet } from 'react-native';
import { Content, View, Text } from 'react-native-biza';

export default function ViewExample() {
  const fadeAnime = useRef(new Animated.Value(1)).current;

  useEffect(() => {
    Animated.loop(Animated.sequence([
      Animated.timing(fadeAnime, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      }),
      Animated.timing(fadeAnime, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }),
    ])).start();
  }, []);

  return (
    <Content scrollable centerContent>
      <View style={styles.view}>
        <Text>animatable: false</Text>
      </View>
      <View animatable style={[styles.view, { opacity: fadeAnime }]}>
      <Text>animatable: true</Text>
      </View>
    </Content>
  );
}

const styles = StyleSheet.create({
  view: {
    padding: 16,
    backgroundColor: 'white',
  },
});
