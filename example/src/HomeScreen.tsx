import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Content, Panel, Layout, View, Text } from 'react-native-biza';

const RightArrow = () => (
  <View>
    <Text weight="bold" color="primary" style={styles.rightArrowTop}>{'_'}</Text>
    <Text weight="bold" color="primary" style={styles.rightArrowBottom}>{'_'}</Text>
  </View>
);

export default function HomeScreen() {
  const navigation = useNavigation();

  return (
    <Content scrollable>
      {[
        'Avatar',
        'Badge',
        'Boundary',
        'Button',
        'Checkbox',
        'CoinText',
        'Content',
        'Divider',
        'Footer',
        'Header',
        'Layer',
        'Layout',
        'Loading',
        'Modal',
        'Number',
        'Panel',
        'Portal',
        'Radio',
        'RatingText',
        'SafeAreaView',
        'Skeleton',
        'Surface',
        'Text',
        'TextInput',
        'Toast',
        'View',
        'Wrapper',
      ].map((example, i) => (
        <TouchableOpacity onPress={() => navigation.navigate(example)} key={i}>
          <Panel>
            <Layout type="row">
              <Layout sizing={1}>
                <Text>{example}</Text>
              </Layout>
              <RightArrow />
            </Layout>
          </Panel>
        </TouchableOpacity>
      ))}
    </Content>
  );
}

const styles = StyleSheet.create({
  rightArrowTop: {
    position: 'absolute',
    right: 4,
    top: 2,
    transform: [{ rotateZ: '225deg' }]
  },
  rightArrowBottom: {
    position: 'absolute',
    right: 4,
    top: -2,
    transform: [{ rotateZ: '315deg' }]
  },
});
