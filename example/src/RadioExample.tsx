import React, { useState } from 'react';

import { Content , Radio } from 'react-native-biza';

export default function RadioExample() {
  const [flag, setFlag] = useState(false);
  
  return (
    <Content centerContent>
      <Radio checked={flag} onPress={() => setFlag(!flag)} />
    </Content>
  );
}
