import React from 'react';
import { Content, CoinText, Text } from 'react-native-biza';

export default function CoinTextExample() {
  return (
    <Content centerContent>
      <CoinText>12,330.0001</CoinText>
      <CoinText size="level5" postfix={<Text size="level6"> BIZA</Text>}>
        <Text weight="bold">23.5</Text>
      </CoinText>
      <CoinText size="level4">12,330.0001</CoinText>
    </Content>
  );
};
