import React from 'react';
import { Alert } from 'react-native';
import { Content, Button, useToastController, Layout, Toast } from 'react-native-biza';

export default function ToastExample() {
  const controller = useToastController();

  return (
    <Content scrollable centerContent>
      <Button
        onPress={() => {
          controller.present(({ id, ...toastProps }) => (
            <Toast
              {...toastProps}
              action="Close"
              onAction={() => {
                Alert.alert('Tip', 'You can write logic for action to here!\nReturn value is control for dismiss.');
                controller.dismiss(id);
              }}
            >
              This is present toast :)
            </Toast>
          ));
        }}
      >
        Present Message
      </Button>
      <Layout marginTop />
      <Button
        onPress={() => {
          controller.present(({ id, ...toastProps }) => (
            <Toast
              {...toastProps}
              action="Close"
              onAction={() => {
                Alert.alert('Tip', 'You can write logic for action to here!\nReturn value is control for dismiss.');
                controller.dismiss(id);
              }}
            >
              This is show toast :)
            </Toast>
          ));
        }}
      >
        Show Message
      </Button>
      <Layout marginTop />
      <Button
        onPress={() => {
          controller.alert('This is show toast :)', { action: 'Close' });
        }}
      >
        Alert Message
      </Button>
    </Content>
  );
}
