import React from 'react';
import { Content, Layout, Boundary, TextInput } from 'react-native-biza';

export default function ContentExample() {
  return (
    <Content scrollable keyboardAvoiding backgroundColor="white">
      {/* @ts-ignore */}
      {(new Array(20)).fill(0).map((v, i) => i + 1).map(n => (
        <Layout padding key={n.toString()}>
          <Boundary>
            <TextInput value={n.toString()} />
          </Boundary>
        </Layout>
      ))}
    </Content>
  );
}
