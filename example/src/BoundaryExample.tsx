import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import { Content, Layout, Surface, Boundary, Button } from 'react-native-biza';

export default function BoundaryExample() {
  const [message, setMessage] = useState('');

  return (
    <Content contentContainerStyle={styles.container}>
      <Layout sizing={1} justify="center" margin>
        <Surface>
          <Layout padding>
            <Boundary message={message.length ? message : null}>
              <Button
                type="transparent"
                onPress={() => setMessage(message.length ? '' : 'Caution!')}
              >
                Toggle Message
            </Button>
            </Boundary>
          </Layout>
        </Surface>
      </Layout>
    </Content>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
