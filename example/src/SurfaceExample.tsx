import React from 'react';
import { StyleSheet } from 'react-native';
import { Content, Surface } from 'react-native-biza';

export default function SurfaceExample() {
  return (
    <Content scrollable centerContent>
      {/* @ts-ignore */}
      {(new Array(5)).fill(0).map((v, i) => i + 1).map(v => (
        <Surface elevation={v} style={styles.box} key={v} />
      ))}
    </Content>
  );
}

const styles = StyleSheet.create({
  box: {
    width: 50,
    height: 50,
    marginVertical: 20,
  },
});
