import React from 'react';
import { Content, Wrapper, Text } from 'react-native-biza';

export default function WrapperExample() {
  return (
    <Content centerContent>
      <Wrapper color="white">
        <Text>color: white</Text>
      </Wrapper>
      <Wrapper outline rounded>
        <Text>outline: true</Text>
        <Text>rounded: true</Text>
      </Wrapper>
    </Content>
  );
}
