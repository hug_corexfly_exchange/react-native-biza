import React from 'react';
import { Content, Layout, Avatar } from 'react-native-biza';

export default function AvatarExample() {
  return (
    <Content scrollable centerContent>
      <Avatar source={{ uri: 'https://www.joongbu.ac.kr/grad/img/contents/box7_12.jpg' }} size="small" />
      <Layout marginVertical>
        <Avatar source={{ uri: 'https://post-phinf.pstatic.net/MjAxODA3MTlfMjM0/MDAxNTMxOTkxODI1MzM4.5xBv_lj_SkKMUEEiKPztLF2rtCsGygYePgZbnUZO5d0g.bc97hKJoiHt0ig15nLR5ZRzWoP6OZ43Lr9QRLVpbqbsg.JPEG/1.jpg?type=w1200' }} size="regular" />
      </Layout>
      <Avatar source={{ uri: 'https://www.sciencetimes.co.kr/wp-content/uploads/2020/02/environmental-education-480x266.jpg' }} size="large" />
    </Content>
  );
}
