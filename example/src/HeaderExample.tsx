import React from 'react';
import { StyleSheet } from 'react-native';
import { Header, Layer, Surface, HeaderBar, Layout, BackButton, Text, Content } from 'react-native-biza';
import { useNavigation } from '@react-navigation/native';

export default function HeaderExample() {
  const navigation = useNavigation();

  return (
    <>
      <Header>
        <Layer type="underlay">
          <Surface style={styles.background} />
        </Layer>
        <HeaderBar>
          <Layout sizing={1} padding>
            <BackButton onPress={() => navigation.goBack()} />
          </Layout>
          <Layout sizing={10}>
            <Text weight="bold" size="level3">Header</Text>
          </Layout>
          <Layout sizing={1} padding />
        </HeaderBar>
      </Header>
      <Content centerContent>
        <Text>Content</Text>
      </Content>
    </>
  );
}

const styles = StyleSheet.create({
  background: { flex: 1 },
});
