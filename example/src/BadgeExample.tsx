import React from 'react';
import { StyleSheet } from 'react-native';
import { Content, Badge, Layout, Text, View } from 'react-native-biza';

export default function BadgeExample() {
  return (
    <Content centerContent>
      <Badge size="small" color="primary">이용/알림</Badge>
      <Layout marginVertical>
        <Badge size="regular" color="#F03E3E" before={<View style={styles.icon} />}>
          <Text weight="bold" color="rgba(255, 255, 255, 0.5)">LIVE </Text>
          <Text color="white">D-5</Text>
        </Badge>
      </Layout>
      <Badge type="outline" size="large" color="#F03E3E">
        <Text color="#F03E3E">LIVE</Text>
      </Badge>
    </Content>
  );
}

const styles = StyleSheet.create({
  icon: {
    width: 6,
    height: 6,
    backgroundColor: 'white',
    borderRadius: 3,
    marginRight: 4,
  },
});
