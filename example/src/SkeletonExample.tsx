import React from 'react';
import { Content, Skeleton } from 'react-native-biza';

export default function SkeletonExample() {
  return (
    <Content centerContent backgroundColor="white">
      <Skeleton width={100} height={100} rounded={100} />
      <Skeleton width={230} height={20} rounded marginTop={32} />
      <Skeleton width={220} height={20} rounded marginTop={8} />
      <Skeleton width={140} height={20} rounded marginTop={8} />
    </Content>
  );
}
