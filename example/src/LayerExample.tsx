import React from 'react';
import { StyleSheet, Image } from 'react-native';
import { Content, Surface, Layout, Layer, Text } from 'react-native-biza';

export default function LayerExample() {
  return (
    <Content centerContent>
      <Surface>
        <Layout padding justify="center" style={styles.box}>
          <Layer type="underlay">
            <Image
              source={{ uri: 'https://cultofthepartyparrot.com/parrots/hd/parrot.gif' }}
              style={styles.image}
            />
          </Layer>
          <Text>This is awesome!</Text>
        </Layout>
      </Surface>
      <Surface>
        <Layout padding justify="center" marginTop style={styles.box}>
          <Text>This is awesome!</Text>
          <Layer type="overlay">
            <Image
              source={{ uri: 'https://cultofthepartyparrot.com/parrots/hd/parrot.gif' }}
              style={styles.image}
            />
          </Layer>
        </Layout>
      </Surface>
    </Content>
  );
}

const styles = StyleSheet.create({
  image: {
    flex: 1,
  },
  box: {
    height: 150,
  },
});
