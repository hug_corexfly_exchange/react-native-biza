# react-native-biza

React Native Biza(RNB) library is React Native UI Kit library for creating a related apps to BIZA ecosystem.

## Installation

```sh
npm install react-native-biza
```

## Usage

```js
import Biza from "react-native-biza";

// ...

const result = await Biza.multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
