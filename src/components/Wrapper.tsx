import React, { FC } from 'react';
import type { ViewStyle } from 'react-native';

import { Layout, LayoutProps } from './Layout';
import { useTheme } from '../hooks';
import type { ThemeColors, ColorsWithTheme } from '../theme';

export type WrapperProps = LayoutProps & {
  color?: ColorsWithTheme;
  rounded?: true | number;
  roundedTopLeft?: true | number;
  roundedTopRight?: true | number;
  roundedBottomLeft?: true | number;
  roundedBottomRight?: true | number;
  outline?: true | number;
  outlineLeft?: true | number;
  outlineRight?: true | number;
  outlineTop?: true | number;
  outlineBottom?: true | number;
  outlineColor?: ColorsWithTheme;
  overflow?: ViewStyle['overflow'];
};

export const Wrapper: FC<WrapperProps> = ({
  color = 'transparent',
  rounded = 0,
  roundedTopLeft = rounded,
  roundedTopRight = rounded,
  roundedBottomLeft = rounded,
  roundedBottomRight = rounded,
  outline = 0,
  outlineLeft = outline,
  outlineRight = outline,
  outlineTop = outline,
  outlineBottom = outline,
  outlineColor = 'border',
  overflow = 'visible',
  style,
  ...otherProps
}) => {
  const { colors, roundness } = useTheme();

  return (
    <Layout
      style={[
        {
          backgroundColor: colors[color as ThemeColors] || color,
          borderTopLeftRadius: roundedTopLeft === true ? roundness : roundedTopLeft,
          borderTopRightRadius: roundedTopRight === true ? roundness : roundedTopRight,
          borderBottomLeftRadius: roundedBottomLeft === true ? roundness : roundedBottomLeft,
          borderBottomRightRadius: roundedBottomRight === true ? roundness : roundedBottomRight,
          borderLeftWidth: outlineLeft === true ? 1 : outlineLeft,
          borderRightWidth: outlineRight === true ? 1 : outlineRight,
          borderTopWidth: outlineTop === true ? 1 : outlineTop,
          borderBottomWidth: outlineBottom === true ? 1 : outlineBottom,
          borderColor: colors[outlineColor as ThemeColors] || outlineColor,
          overflow,
        },
        style,
      ]}
      {...otherProps}
    />
  );
};
