import React, { FC, ComponentType, ReactNode } from 'react';
import { StyleSheet, ViewProps } from 'react-native';

import { View } from './View';
import { Text, TextProps } from './Text';
import { useTheme } from '../hooks';
import type { ThemeColors } from '../theme';
import DiamondOutlineSvg from '../assets/diamond-outline.svg';
import DiamondSvg from '../assets/diamond.svg';

export type CoinIconProps = ViewProps & {
  size: number;
  color: string;
};

const DefaultCoinIcon: FC<CoinIconProps> = ({ size, color, ...otherProps }) => (
  size < 16 ? (
    <DiamondOutlineSvg width={size} height={size} fill={color} {...otherProps} />
  ) : (
    <DiamondSvg width={size} height={size} fill={color} {...otherProps} />
  )
);

export type CoinTextProps = TextProps & {
  Icon?: ComponentType<CoinIconProps>;
  postfix?: ReactNode;
  textStyle?: TextProps['style'];
};

export const CoinText: FC<CoinTextProps> = ({
  Icon = DefaultCoinIcon,
  postfix = ' BIZA',
  font = 'Default',
  size = 'level6',
  color = 'accent',
  textStyle,
  style,
  children,
  ...otherProps
}) => {
  const { fonts, colors } = useTheme();
  const iconSize = typeof size === 'string' ? fonts[font].size[size].fontSize : size;

  return (
    <View style={[styles.wrapper, style]}>
      <Icon size={iconSize} color={colors[color as ThemeColors] || color} style={styles.icon} />
      <Text
        font={font}
        size={size}
        weight={iconSize < 16 ? 'regular' : 'bold'}
        color={color}
        style={textStyle}
        {...otherProps}
      >
        {children}
        {postfix}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: { marginRight: 2 },
});
