import React, { FC, ComponentType } from 'react';
import { StyleSheet, ViewProps } from 'react-native';

import { View } from './View';
import { Text, TextProps } from './Text';
import { useTheme } from '../hooks';
import type { ThemeColors } from '../theme';
import { yellow5 } from '../theme';
import StarSvg from '../assets/star.svg';

export type RatingIconProps = ViewProps & {
  size: number;
  color: string;
};

const DefaultRatingIcon: FC<RatingIconProps> = ({ size, color, ...otherProps }) => {
  const iconSize = size <= 14 ? size - 2 : size;
  
  return (
    <StarSvg width={iconSize} height={iconSize} fill={color} {...otherProps} />
  );
}

export type RatingTextProps = TextProps & {
  Icon?: ComponentType<RatingIconProps>;
  textStyle?: TextProps['style'];
};

export const RatingText: FC<RatingTextProps> = ({
  Icon = DefaultRatingIcon,
  font = 'Default',
  size = 'level5',
  color = yellow5,
  textStyle,
  style,
  ...otherProps
}) => {
  const { fonts, colors } = useTheme();
  const iconSize = typeof size === 'string' ? fonts[font].size[size].fontSize : size;
  const iconStyle = { marginRight: iconSize / 4, bottom: 1 };

  return (
    <View style={[styles.wrapper, style]}>
      <Icon size={iconSize} color={colors[color as ThemeColors] || color} style={iconStyle} />
      <Text font={font} size={size} style={textStyle} {...otherProps} />
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
