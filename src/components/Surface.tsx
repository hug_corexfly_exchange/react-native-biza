import React, { FC } from 'react';
import { StyleSheet, ViewStyle } from 'react-native';

import { View, ViewProps } from './View';
import { useTheme } from '../hooks';

export type SurfaceProps = ViewProps & {
  elevation?: number;
};

export const Surface: FC<SurfaceProps> = ({
  elevation: elevationProp = 1,
  style = {},
  ...otherProps
}: SurfaceProps) => {
  const { colors } = useTheme();
  const {
    elevation = elevationProp,
    backgroundColor = colors.surface,
    zIndex = 9,
    ...otherStyle
  } = StyleSheet.flatten(style as ViewStyle);

  return (
    <View
      style={[
        { backgroundColor },
        otherStyle,
        { elevation: elevation },
        elevation > 0 && {
          shadowOffset: {
            width: 0,
            height: Math.max((elevation - 1) * 0.6, 0.25),
          },
          shadowRadius: Math.max(elevation * 0.6, 1),
          shadowOpacity: 1,
          shadowColor: `rgba(0, 0, 0, .3)`,
          zIndex,
        }
      ]}
      {...otherProps}
    />
  );
};
