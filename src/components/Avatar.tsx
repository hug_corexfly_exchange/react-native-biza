import React, { FC } from 'react';
import { StyleSheet, Image, ImageProps } from 'react-native';

export type AvatarProps = ImageProps & {
  size: 'small' | 'regular' | 'large';
};

export const Avatar: FC<AvatarProps> = ({ size = 'regular', source, style, ...otherProps }) => (
  <Image source={source} style={[sizeStyles[size], style]} {...otherProps} />
);

const SMALL_SIZE = 24;
const REGULAR_SIZE = 56;
const LARGE_SIZE = 64;
const sizeStyles = StyleSheet.create({
  small: {
    width: SMALL_SIZE,
    height: SMALL_SIZE,
    borderRadius: SMALL_SIZE,
  },
  regular: {
    width: REGULAR_SIZE,
    height: REGULAR_SIZE,
    borderRadius: REGULAR_SIZE,
  },
  large: {
    width: LARGE_SIZE,
    height: LARGE_SIZE,
    borderRadius: LARGE_SIZE,
  },
});
