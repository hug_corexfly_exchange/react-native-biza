import React, { FC } from 'react';
import { StyleSheet } from 'react-native';

import { View, ViewProps } from './View';
import { useTheme } from '../hooks';

export type ModalButtonsProps = ViewProps;

export const ModalButtons: FC<ModalButtonsProps> = ({ style, ...otherProps }) => {
  const { colors } = useTheme();

  return (
    <View
      style={[
        {
          backgroundColor: colors.white,
          borderColor: colors.edge,
        },
        styles.wrapper,
        style,
      ]}
      {...otherProps}
    />
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    borderTopWidth: 1,
  },
});
