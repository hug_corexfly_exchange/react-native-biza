import React, { FC, ReactNode } from 'react';
import { StyleSheet, StyleProp, TextStyle } from 'react-native';

import { Layer } from './Layer';
import { SafeAreaView } from './SafeAreaView';
import { Text } from './Text';
import { VisibleAnimation, VisibleAnimationProps } from './VisibleAnimation';
import { View, ViewProps } from './View';
import { useTheme } from '../hooks';
import { TouchableView } from './TouchableView';

export type ToastProps = VisibleAnimationProps & {
  visible: boolean;
  action?: ReactNode;
  onAction?: () => void;
  barStyle?: ViewProps['style'];
  textStyle?: StyleProp<TextStyle>;
};

export const Toast: FC<ToastProps> = ({
  type = 'slideFromBottomWithFade',
  visible,
  onShow,
  onShown,
  onHide,
  onHidden,
  action,
  onAction,
  barStyle,
  textStyle,
  style,
  children,
  ...otherProps
}) => {
  const { roundness, layouts, toast } = useTheme();

  return (
    <Layer left={0} right={0} bottom={0} style={style}>
      <VisibleAnimation
        type={type}
        visible={visible}
        onShow={onShow}
        onShown={onShown}
        onHide={onHide}
        onHidden={onHidden}
      >
        <SafeAreaView>
          <View
            style={[
              styles.wrapper,
              {
                padding: layouts.padding,
                backgroundColor: toast.background,
                borderRadius: roundness,
                margin: layouts.padding,
              },
              barStyle,
            ]}
            {...otherProps}
          >
            <Text color={toast.color} style={[{ flex: action ? 1 : 0 }, textStyle]}>{children}</Text>
            {action && (
              <TouchableView onPress={onAction}>
                <Text weight="bold" color={toast.color}>{action}</Text>
              </TouchableView>
            )}
          </View>
        </SafeAreaView>
      </VisibleAnimation>
    </Layer>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    flex: 1,
  },
});
