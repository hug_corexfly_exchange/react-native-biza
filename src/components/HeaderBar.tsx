import React, { FC } from 'react';
import { StyleSheet } from 'react-native';

import { View, ViewProps } from './View';
import { useTheme } from '../hooks';

export type HeaderBarProps = ViewProps;

export const HeaderBar: FC<HeaderBarProps> = ({ style, ...otherProps }) => {
  const { layouts } = useTheme();

  return <View style={[{ minHeight: layouts.header }, styles.wrapper, style]} {...otherProps} />;
}

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
