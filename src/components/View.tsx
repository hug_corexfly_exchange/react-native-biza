import React, { FC } from 'react';
import { View as RNView, ViewProps as RNViewProps, StyleProp, ViewStyle, Animated } from 'react-native';

export type ViewProps = (RNViewProps & {
  animatable: true;
  style?: Animated.WithAnimatedValue<StyleProp<ViewStyle>>;
}) | (RNViewProps & {
  animatable?: false;
  style?: StyleProp<ViewStyle>;
});

export const View: FC<ViewProps> = ({ animatable = false, ...otherProps }) => (
  animatable ? <Animated.View {...otherProps} /> : <RNView {...otherProps} />
);
