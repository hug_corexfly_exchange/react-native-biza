import React, { FC } from 'react';
import { View, ViewProps } from 'react-native';

import { SafeAreaView } from './SafeAreaView';

export type FooterProps = ViewProps & {
  safeArea?: 'auto' | 'always';
};

export const Footer: FC<FooterProps> = ({ safeArea = 'auto', ...otherProps }) => safeArea ? (
  <SafeAreaView
    inset={safeArea === 'auto' ? safeArea : 'bottom'}
    {...otherProps}
  />
) : (
  <View {...otherProps} />
);
