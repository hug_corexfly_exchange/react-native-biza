import React, { FC, useMemo } from 'react';

import { Toast } from './Toast';
import { ToastControllerContext } from '../contexts';
import { useToastManager } from '../hooks';

export const ToastProvider: FC = ({ children }) => {
  const manager = useToastManager();
  const context = useMemo(() => ({
    present: manager.present,
    show: manager.show,
    alert: manager.alert,
    dismiss: manager.dismiss,
  }), []);

  return (
    <ToastControllerContext.Provider value={context}>
      {children}
      {manager.render(({ id, visible, message: Message, action, onAction }) => (
        typeof Message === 'function' ? (
          // @ts-ignore
          <Message id={id} visible={visible} onHidden={() => manager.dispose(id)} key={id} />
        ) : (
          <Toast
            visible={visible}
            onHidden={() => manager.dispose(id)}
            action={action}
            onAction={() => {
              onAction?.();
              manager.dismiss(id);
            }}
            key={id}
          >
            {Message}
          </Toast>
        )
      ))}
    </ToastControllerContext.Provider>
  );
};
