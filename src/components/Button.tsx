import React, { ComponentType, FC, ReactNode } from 'react';
import { StyleProp, StyleSheet, View, ViewStyle } from 'react-native';

import { TouchableView, TouchableViewProps } from './TouchableView';
import { Text } from './Text';
import { useTheme } from '../hooks';
import type { ThemeColors, ColorsWithTheme } from '../theme';

export type ButtonProps = TouchableViewProps & {
  type?: 'transparent' | 'filled' | 'outline';
  size?: 'tiny' | 'small' | 'regular' | 'large';
  color?: ColorsWithTheme;
  block?: boolean;
  rounded?: true | number;
  disabled?: boolean;
  before?: ReactNode;
  after?: ReactNode;
  Background?: ComponentType<{ style?: StyleProp<ViewStyle> }>;
};

export const Button: FC<ButtonProps> = ({
  type = 'filled',
  size = 'regular',
  color = 'primary',
  block = false,
  rounded = true,
  disabled = false,
  before,
  after,
  Background = View,
  pointerEvents,
  style,
  children,
  ...otherProps
}) => {
  const { colors, forms, layouts, roundness } = useTheme();
  const selectedColor = colors[color as ThemeColors] || color;
  let builtInStyle: ViewStyle = { paddingHorizontal: layouts.padding };
  let backgroundStyle: ViewStyle = {};
  let textColor: ColorsWithTheme = 'text';
  if (block) builtInStyle.width = '100%';
  if (rounded) backgroundStyle.borderRadius = rounded === true ? roundness : rounded;
  if (disabled) {
    backgroundStyle.backgroundColor = forms.button.muted.background;
    textColor = forms.button.muted.text;
  } else if (type === 'filled') {
    backgroundStyle.backgroundColor = selectedColor;
    textColor = 'white';
  } else if (type === 'outline') {
    backgroundStyle.borderWidth = 1;
    backgroundStyle.borderColor = selectedColor;
  }

  return (
    <TouchableView
      style={[styles.container, sizeStyles[size], builtInStyle, style]}
      pointerEvents={disabled ? 'none' : pointerEvents}
      {...otherProps}
    >
      <Background style={[styles.background, backgroundStyle]} />
      {before}
      <Text
        size={['regular', 'large'].includes(size) ? 'level4' : 'level5'}
        weight={forms.button.font.wegith}
        color={textColor}
      >
        {children}
      </Text>
      {after}
    </TouchableView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  background: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
});

const sizeStyles = StyleSheet.create({
  tiny: { height: 36 },
  small: { height: 40 },
  regular: { height: 48 },
  large: { height: 56 },
});
