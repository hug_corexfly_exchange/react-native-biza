import React, { FC } from 'react';
import { StyleSheet } from 'react-native';

import { TouchableView, TouchableViewProps } from './TouchableView';
import { useTheme } from '../hooks';
import type { ThemeColors, ColorsWithTheme } from '../theme';
import CheckboxOnSvg from '../assets/checkbox-on.svg';
import CheckboxOffSvg from '../assets/checkbox-off.svg';

export type CheckboxProps = TouchableViewProps & {
  size?: 'small' | 'regular' | 'large'
  checked?: boolean;
  checkedColor?: ColorsWithTheme;
  uncheckedColor?: ColorsWithTheme;
};

export const Checkbox: FC<CheckboxProps> = ({
  size = 'regular',
  checked = false,
  checkedColor = 'primary',
  uncheckedColor,
  onPress,
  style,
  ...otherProps
}) => {
  const { colors, forms } = useTheme();
  const selectedCheckedColor = colors[checkedColor as ThemeColors] || checkedColor;
  const selectedUncheckedColor = uncheckedColor ? (
    colors[uncheckedColor as ThemeColors] || uncheckedColor
  ) : forms.checkbox.unchecked;

  return (
    <TouchableView onPress={onPress} style={[styles[size], style]} {...otherProps}>
      {checked ? (
        <CheckboxOnSvg width="100%" height="100%" fill={selectedCheckedColor} />
      ) : (
        <CheckboxOffSvg width="100%" height="100%" fill={selectedUncheckedColor} />
      )}
    </TouchableView>
  );
};

const CHECK_BOX_SMALL_SIZE = 14;
const CHECK_BOX_REGULAR_SIZE = 18;
const CHECK_BOX_LARGE_SIZE = 22;
const styles = StyleSheet.create({
  small: {
    width: CHECK_BOX_SMALL_SIZE,
    height: CHECK_BOX_SMALL_SIZE,
  },
  regular: {
    width: CHECK_BOX_REGULAR_SIZE,
    height: CHECK_BOX_REGULAR_SIZE,
  },
  large: {
    width: CHECK_BOX_LARGE_SIZE,
    height: CHECK_BOX_LARGE_SIZE,
  },
});
