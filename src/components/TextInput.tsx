import React, { FC, useRef, useState } from 'react';
import {
  View,
  ViewProps,
  TextInput as RNTextInput,
  TextInputProps as RNTextInputProps,
  TextStyle,
  TouchableOpacity,
} from 'react-native';

import { useTheme } from '../hooks';
import type { ThemeFonts, ThemeFontSizes, ThemeFontWeights, ThemeColors, ColorsWithTheme } from '../theme';
import SearchClearSvg from '../assets/search-clear.svg';

type ClearButtonProps = ViewProps & {
  onPress?: () => void;
};

const ClearButton: FC<ClearButtonProps> = ({ onPress, style, ...otherProps }) => (
  <TouchableOpacity onPress={onPress} style={style}>
    <SearchClearSvg {...otherProps} />
  </TouchableOpacity>
);

export type TextInputProps = Omit<RNTextInputProps, 'clearButtonMode'> & {
  clearButtonMode?: RNTextInputProps['clearButtonMode'] | 'while-has-value';
  font?: ThemeFonts;
  weight?: ThemeFontWeights;
  size?: ThemeFontSizes;
  color?: ColorsWithTheme;
  placeholderTextColor?: ColorsWithTheme;
  textInputStyle?: RNTextInputProps['style'];
};

export const TextInput: FC<TextInputProps> = ({
  font = 'Default',
  weight = 'default',
  size = 'default',
  color = 'text',
  editable = true,
  autoCapitalize = 'none',
  clearButtonMode = 'never',
  value,
  onFocus,
  onBlur,
  onChangeText,
  placeholderTextColor = 'placeholder',
  textInputStyle,
  style,
  ...otherProps
}) => {
  const { layouts, colors, fonts, forms } = useTheme();
  const textInput = useRef<RNTextInput>(null);
  const [focused, setFocused] = useState(false);

  const containerStyle = {
    justifyContent: 'center' as const,
    backgroundColor: editable ? undefined : forms.input.muted.background,
  };
  const inputStyle: TextStyle = {
    height: 48,
    paddingHorizontal: layouts.padding,
    ...fonts[font].weight[weight],
    ...fonts[font].size[size],
    color: editable ? (
      colors[color as ThemeColors] || color
    ) : (
      forms.input.muted.text
    ),
  };
  const clearButtonStyle = {
    position: 'absolute' as const,
    right: layouts.padding,
  };

  const mustShowClearButton = (clearButtonMode === 'while-editing' && focused) ||
    (clearButtonMode === 'unless-editing' && !focused) ||
    (clearButtonMode === 'while-has-value' && value && value.length) ||
    clearButtonMode === 'always';
  if (mustShowClearButton) {
    inputStyle.paddingRight = layouts.padding * 3;
  }

  const clearText = () => {
    onChangeText?.('');
    textInput.current?.focus();
  };

  return (
    <View style={[containerStyle, style]}>
      <RNTextInput
        ref={textInput}
        editable={editable}
        autoCapitalize={autoCapitalize}
        value={value}
        onFocus={event => {
          setFocused(true);
          onFocus?.(event);
        }}
        onBlur={event => {
          setFocused(false);
          onBlur?.(event);
        }}
        onChangeText={onChangeText}
        placeholderTextColor={colors[placeholderTextColor as ThemeColors] || placeholderTextColor}
        style={[inputStyle, textInputStyle]}
        {...otherProps}
      />
      {mustShowClearButton && (
        <ClearButton onPress={clearText} style={clearButtonStyle} />
      )}
    </View>
  );
};
