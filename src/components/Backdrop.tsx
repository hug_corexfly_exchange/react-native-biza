import React, { FC } from 'react';
import { StyleSheet, TouchableWithoutFeedback } from 'react-native';

import { Layer, LayerProps } from './Layer';
import { useTheme } from '../hooks';
import { VisibleAnimation } from './VisibleAnimation';

export type BackdropProps = LayerProps & {
  visible: boolean;
  onPress?: () => void;
};

export const Backdrop: FC<BackdropProps> = ({ visible, onPress, style, ...otherProps }) => {
  const { colors } = useTheme();

  return (
    <VisibleAnimation visible={visible} style={styles.animation} pointerEvents="box-none">
      <TouchableWithoutFeedback onPress={onPress}>
        <Layer
          type="underlay"
          style={[{ backgroundColor: colors.backdrop }, style]}
          {...otherProps}
        />
      </TouchableWithoutFeedback>
    </VisibleAnimation>
  );
};

const styles = StyleSheet.create({
  animation: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    left: 0,
    top: 0,
  },
})
