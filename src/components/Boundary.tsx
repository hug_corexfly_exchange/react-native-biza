import React, { ComponentType, FC, ReactNode } from 'react';
import { StyleSheet, StyleProp, ViewStyle } from 'react-native';

import { View, ViewProps } from './View';
import { Text } from './Text';
import { useTheme } from '../hooks';
import type { ThemeColors, ColorsWithTheme } from '../theme';
import CautionSvg from '../assets/caution.svg';

type MessageWrapperProps = {
  color: string
};

export type BoundaryProps = ViewProps & {
  outline?: number;
  outlineLeft?: number;
  outlineRight?: number;
  outlineTop?: number;
  outlineBottom?: number;
  rounded?: number;
  roundedTopLeft?: number;
  roundedTopRight?: number;
  roundedBottomLeft?: number;
  roundedBottomRight?: number;
  color?: ColorsWithTheme;
  message?: ReactNode,
  standoutColor?: ColorsWithTheme,
  MessageWrapper?: ComponentType<MessageWrapperProps>;
  wrapperStyle?: StyleProp<ViewStyle>;
  contentContainerStyle?: StyleProp<ViewStyle>;
};

const ICON_SIZE = 16;
const DefaultMessageWrapper: FC<MessageWrapperProps> = ({ color, children, ...otherProps }) => (
  <View style={styles.messageWrapper} {...otherProps}>
    <CautionSvg width={ICON_SIZE} height={ICON_SIZE} fill={color} style={styles.messageIcon} />
    {children}
  </View>
);

export const Boundary: FC<BoundaryProps> = ({
  outline = 1,
  outlineLeft = outline,
  outlineRight = outline,
  outlineTop = outline,
  outlineBottom = outline,
  rounded,
  roundedTopLeft = rounded,
  roundedTopRight = rounded,
  roundedBottomLeft = rounded,
  roundedBottomRight = rounded,
  color = 'border',
  message,
  standoutColor = 'caution',
  MessageWrapper = DefaultMessageWrapper,
  wrapperStyle,
  contentContainerStyle,
  style,
  children,
  ...otherProps
}) => {
  const { colors, roundness } = useTheme();
  const selectedStandoutColor = colors[standoutColor as ThemeColors] || standoutColor;
  const builtInContentContainerStyle = {
    borderTopLeftRadius: roundedTopLeft ?? (outlineTop && outlineLeft ? roundness : 0),
    borderTopRightRadius: roundedTopRight ?? (outlineTop && outlineRight ? roundness : 0),
    borderBottomLeftRadius: roundedBottomLeft ?? (outlineBottom && outlineLeft ? roundness : 0),
    borderBottomRightRadius: roundedBottomRight ?? (outlineBottom && outlineRight ? roundness : 0),
    overflow: 'hidden' as const,
  };
  const builtInWrapperStyle = {
    borderLeftWidth: outlineLeft,
    borderRightWidth: outlineRight,
    borderTopWidth: outlineTop,
    borderBottomWidth: outlineBottom,
    borderColor: message != null && message !== false ?
      selectedStandoutColor : colors[color as ThemeColors] || color,
    ...builtInContentContainerStyle,
  };

  return (
    <View style={style}>
      <View style={[builtInWrapperStyle, wrapperStyle]}>
        <View style={[builtInContentContainerStyle, contentContainerStyle]} {...otherProps}>
          {children}
        </View>
      </View>
      {message !== '' && message !== true && message && (
        <MessageWrapper color={selectedStandoutColor}>
          <Text size="level6" color={selectedStandoutColor} style={styles.message}>
            {message}
          </Text>
        </MessageWrapper>
      )}
    </View>
  );
};

const ICON_MARGIN = 5;
const styles = StyleSheet.create({
  messageWrapper: {
    flexDirection: 'row',
    marginTop: ICON_MARGIN,
  },
  messageIcon: {
    marginRight: ICON_MARGIN,
    marginTop: ICON_MARGIN * 0.25,
  },
  message: { flex: 1 },
});
