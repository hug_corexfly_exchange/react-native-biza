import React, { FC, useContext, useEffect } from 'react';

import { PortalContext } from '../contexts';

export type PortalProps = {};

export const Portal: FC<PortalProps> = ({ children }) => {
  const setElement = useContext(PortalContext);

  useEffect(() => {
    setElement(children);
    return () => { setElement(null); };
  });

  return <></>;
};
