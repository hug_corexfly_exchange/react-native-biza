import React, { FC, useEffect, useRef, useState } from 'react';
import { ViewProps, View, Dimensions, Animated, Insets } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import { useTheme } from '../hooks';

export type SafeAreaViewProps = ViewProps & {
  inset?: 'auto' | 'horizontal' | 'vertical' | 'left' | 'right' | 'top' | 'bottom';
  threshold?: number;
};

export const SafeAreaView: FC<SafeAreaViewProps> = ({
  inset = 'auto',
  threshold = 60,
  onLayout,
  style,
  ...otherProps
}) => {
  const viewRef = useRef<View>(null);
  const insets = useSafeAreaInsets();
  const { animation } = useTheme();

  const getAnimationCommonConfig = (animated: boolean) => ({
    duration: animated ? animation : 0,
    useNativeDriver: false,
  });

  const [layoutCount, setLayoutCount] = useState(0);
  const measureRef = useRef({ x: 0, y: 0, width: 0, height: 0 });
  const [contentInset, setContentInset] = useState<Required<Insets> | null>(null);
  const contentInsetLeft = useRef(
    new Animated.Value(inset === 'horizontal' || inset === 'left' ? insets.left : 0)
  ).current;
  const contentInsetRight = useRef(
    new Animated.Value(inset === 'horizontal' || inset === 'right' ? insets.right : 0)
  ).current;
  const contentInsetTop = useRef(
    new Animated.Value(inset === 'vertical' || inset === 'top' ? insets.top : 0)
  ).current;
  const contentInsetBottom = useRef(
    new Animated.Value(inset === 'vertical' || inset === 'bottom' ? insets.bottom : 0)
  ).current;
  useEffect(() => {
    if (inset !== 'auto') return;

    const interval = 1000 / threshold;
    let timeout: NodeJS.Timeout | null = null;
    const computeMeasure = () => {
      viewRef.current?.measureInWindow((x, y, width, height) => {
        const { current } = measureRef;
        if (x !== current.x || y !== current.y ||
          width !== current.width || height !== current.height) {
          measureRef.current = { x, y, width, height };
          timeout = setTimeout(computeMeasure, interval);
          return;
        }

        const window = Dimensions.get('window');
        const { left, right, top, bottom } = insets;
        setContentInset({
          left: current.x <= 0 ? left : 0,
          right: current.x + current.width >= window.width ? right : 0,
          top: current.y <= 0 ? top : 0,
          bottom: current.y + current.height >= window.height ? bottom : 0,
        });
        timeout = null;
      });
    };

    computeMeasure();
    return () => {
      if (timeout) clearTimeout(timeout);
    };
  }, [inset, layoutCount, threshold]);

  useEffect(() => {
    const { left, right, top, bottom } = insets;
    switch (inset) {
      case 'horizontal':
        setContentInset({ ...insets, top: 0, bottom: 0 });
        break;
      case 'vertical':
        setContentInset({ ...insets, left: 0, right: 0 });
        break;
      case 'left':
        setContentInset({ left, right: 0, top: 0, bottom: 0 });
        break;
      case 'right':
        setContentInset({ left: 0, right, top: 0, bottom: 0 });
        break;
      case 'top':
        setContentInset({ left: 0, right: 0, top, bottom: 0 });
        break;
      case 'bottom':
        setContentInset({ left: 0, right: 0, top: 0, bottom });
        break;
      default:
        break;
    }
  }, [inset, insets]);

  const applyCount = useRef(0);
  const isAnimating = useRef(false);
  useEffect(() => {
    if (!contentInset) return;

    isAnimating.current = true;
    const { left, right, top, bottom } = contentInset;
    Animated.parallel([
      Animated.timing(contentInsetLeft, {
        toValue: left,
        ...getAnimationCommonConfig(applyCount.current > 0),
      }),
      Animated.timing(contentInsetRight, {
        toValue: right,
        ...getAnimationCommonConfig(applyCount.current > 0),
      }),
      Animated.timing(contentInsetTop, {
        toValue: top,
        ...getAnimationCommonConfig(applyCount.current > 0),
      }),
      Animated.timing(contentInsetBottom, {
        toValue: bottom,
        ...getAnimationCommonConfig(applyCount.current > 0),
      }),
    ], { stopTogether: true }).start(() => {
      applyCount.current++;
      isAnimating.current = false;
    });
  }, [contentInset]);

  return (
    <Animated.View
      ref={viewRef}
      onLayout={() => {
        if (!isAnimating.current) setLayoutCount(prev => prev + 1);
      }}
      style={[
        style,
        {
          paddingLeft: contentInsetLeft,
          paddingRight: contentInsetRight,
          paddingTop: contentInsetTop,
          paddingBottom: contentInsetBottom,
        },
      ]}
      {...otherProps}
    />
  );
};
