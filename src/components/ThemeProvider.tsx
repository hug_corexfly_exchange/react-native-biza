import React, { FC } from 'react';

import { ThemeContext } from '../contexts';
import { DefaultTheme } from '../theme';

export type ThemeProviderProps = {
  value?: typeof DefaultTheme;
}

export const ThemeProvider: FC<ThemeProviderProps> = props => (
  <ThemeContext.Provider value={DefaultTheme} {...props} />
);
