import React, { FC, useMemo, useRef, useState } from 'react';
import { LayoutAnimation } from 'react-native';

import { View, ViewProps } from './View';
import { useTheme } from '../hooks';

export type VisibleAnimationProps = ViewProps & {
  type?: 'fade' | 'slideFromBottom' | 'slideFromBottomWithFade';
  visible: boolean;
  onShow?: () => void;
  onShown?: () => void;
  onHide?: () => void;
  onHidden?: () => void;
};

const createVisbileAnimationConfig = (duration: number) => ({
  duration,
  create: {
    type: LayoutAnimation.Types.easeInEaseOut,
    property: LayoutAnimation.Properties.opacity,
  },
  update: {
    type: LayoutAnimation.Types.easeInEaseOut,
  },
  delete: {
    type: LayoutAnimation.Types.easeInEaseOut,
    property: LayoutAnimation.Properties.opacity,
  },
});

export const VisibleAnimation: FC<VisibleAnimationProps> = ({
  type = 'fadeIn',
  visible,
  onShow,
  onShown,
  onHide,
  onHidden,
  style,
  children,
  ...otherProps
}) => {
  const { animation } = useTheme();

  const [hasAnimating, setHasAnimating] = useState(false);
  const animationCountRef = useRef(0);

  useMemo(() => {
    if (animationCountRef.current++ === 0) return;

    LayoutAnimation.configureNext(createVisbileAnimationConfig(animation));

    setHasAnimating(true);
    if (visible) {
      onShow?.();
    } else {
      onHide?.();
    }

    setTimeout(() => {
      setHasAnimating(false);
      if (visible) {
        onShown?.();
      } else {
        onHidden?.();
      }
    }, animation);
  }, [visible]);

  return (
    <View
      pointerEvents={hasAnimating ? 'box-only' : (visible ? 'box-none' : 'none')}
      style={[
        style,
        type.includes('FromBottom') ? {
          top: visible ? '0%' : '100%',
        } : {},
      ]}
      {...otherProps}
    >
      {type.match('fade|Fade') ? (
        visible && children
      ) : (
        children
      )}
    </View>
  );
}
