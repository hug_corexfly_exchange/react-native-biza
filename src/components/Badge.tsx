import React, { FC, ReactNode } from 'react';
import { StyleSheet, ViewStyle } from 'react-native';

import { View, ViewProps } from './View';
import { Text } from './Text';
import { useTheme } from '../hooks';
import type { ThemeColors, ColorsWithTheme } from '../theme';

export type BadgeProps = ViewProps & {
  type?: 'filled' | 'outline';
  size?: 'small' | 'regular' | 'large';
  color?: ColorsWithTheme;
  before?: ReactNode;
  after?: ReactNode;
};

export const Badge: FC<BadgeProps> = ({
  type = 'filled',
  size = 'regular',
  color = 'primary',
  before,
  after,
  style,
  children,
  ...otherProps
}) => {
  const { colors } = useTheme();
  const selectedColor = colors[color as ThemeColors] || color;
  let builtInStyle: ViewStyle = {};
  let textColor: ColorsWithTheme = 'text';
  if (type === 'filled') {
    builtInStyle.backgroundColor = selectedColor;
    textColor = 'white';
  } else {
    builtInStyle.borderWidth = 1;
    builtInStyle.borderColor = selectedColor;
  }

  return (
    <View style={[styles.container, sizeStyles[size], builtInStyle, style]} {...otherProps}>
      {before}
      <Text size={FontSizes[size]} color={textColor}>
        {children}
      </Text>
      {after}
    </View>
  );
};

const FontSizes = {
  small: 'level7' as const,
  regular: 'level6' as const,
  large: 'level5' as const,
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

const sizeStyles = StyleSheet.create({
  small: {
    height: 16,
    borderRadius: 2,
    paddingHorizontal: 4,
  },
  regular: {
    height: 19,
    borderRadius: 6,
    paddingHorizontal: 6,
  },
  large: {
    height: 25,
    borderRadius: 12.5,
    paddingHorizontal: 10,
  },
});
