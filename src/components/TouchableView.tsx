import React, { FC, useRef } from 'react';
import { TouchableWithoutFeedback, Animated } from 'react-native';

import { View, ViewProps } from './View';
import { useTheme } from '../hooks';

export type TouchableViewProps = ViewProps & {
  onPress?: () => void;
  activeOpacity?: number;
};

export const TouchableView: FC<TouchableViewProps> = ({
  onPress,
  activeOpacity = 0.3,
  style,
  ...otherProps
}) => {
  const { animation } = useTheme();
  const value = useRef(new Animated.Value(1)).current;
  const opacityStyle = useRef({ opacity: value }).current;
  const pressInDuration = useRef(animation * 0.25).current;
  const handlePressIn = () => {
    Animated.timing(value, {
      toValue: activeOpacity,
      duration: pressInDuration,
      useNativeDriver: true,
    }).start();
  };
  const handlePressOut = () => {
    Animated.timing(value, {
      toValue: 1,
      duration: animation,
      useNativeDriver: true,
    }).start();
  };

  return (
    <TouchableWithoutFeedback
      onPress={onPress}
      onPressIn={handlePressIn}
      onPressOut={handlePressOut}
    >
      <View {...otherProps} animatable style={[style, opacityStyle]} />
    </TouchableWithoutFeedback>
  );
};
