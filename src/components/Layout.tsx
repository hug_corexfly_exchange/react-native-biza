import React, { FC } from 'react';
import type { FlexStyle } from 'react-native';

import { View, ViewProps } from './View';
import { useTheme } from '../hooks';

export type LayoutProps = ViewProps & {
  sizing?: FlexStyle['flex'];
  type?: FlexStyle['flexDirection'];
  wrap?: FlexStyle['flexWrap'];
  justify?: FlexStyle['justifyContent'];
  alignItems?: FlexStyle['alignItems'];
  alignContent?: FlexStyle['alignContent'];
  alignSelf?: FlexStyle['alignSelf'];
  minWidth?: FlexStyle['minWidth'];
  maxWidth?: FlexStyle['maxWidth'];
  width?: FlexStyle['width'];
  minHeight?: FlexStyle['minHeight'];
  maxHeight?: FlexStyle['maxHeight'];
  height?: FlexStyle['height'];
  padding?: true | FlexStyle['padding'];
  paddingHorizontal?: true | FlexStyle['paddingHorizontal'];
  paddingVertical?: true | FlexStyle['paddingVertical'];
  paddingLeft?: true | FlexStyle['paddingLeft'];
  paddingRight?: true | FlexStyle['paddingRight'];
  paddingTop?: true | FlexStyle['paddingTop'];
  paddingBottom?: true | FlexStyle['paddingBottom'];
  margin?: true | FlexStyle['margin'];
  marginHorizontal?: true | FlexStyle['marginHorizontal'];
  marginVertical?: true | FlexStyle['marginVertical'];
  marginLeft?: true | FlexStyle['marginLeft'];
  marginRight?: true | FlexStyle['marginRight'];
  marginTop?: true | FlexStyle['marginTop'];
  marginBottom?: true | FlexStyle['marginBottom'];
  debug?: boolean;
};

export const Layout: FC<LayoutProps> = ({
  sizing = 0,
  type = 'column',
  wrap = 'nowrap',
  justify = 'flex-start',
  alignItems = 'stretch',
  alignContent = 'stretch',
  alignSelf = 'auto',
  minWidth,
  maxWidth,
  width,
  minHeight,
  maxHeight,
  height,
  padding,
  paddingHorizontal,
  paddingVertical,
  paddingLeft,
  paddingRight,
  paddingTop,
  paddingBottom,
  margin,
  marginHorizontal,
  marginVertical,
  marginLeft,
  marginRight,
  marginTop,
  marginBottom,
  debug = false,
  style,
  ...otherProps
}) => {
  const { layouts } = useTheme();

  return (
    <View
      style={[
        {
          flex: sizing,
          flexDirection: type,
          flexWrap: wrap,
          justifyContent: justify,
          alignItems,
          alignContent,
          alignSelf,
          minWidth,
          maxWidth,
          width,
          minHeight,
          maxHeight,
          height,
          padding: padding === true ? layouts.padding : padding,
          paddingHorizontal: paddingHorizontal === true ? layouts.padding : paddingHorizontal,
          paddingVertical: paddingVertical === true ? layouts.padding : paddingVertical,
          paddingLeft: paddingLeft === true ? layouts.padding : paddingLeft,
          paddingRight: paddingRight === true ? layouts.padding : paddingRight,
          paddingTop: paddingTop === true ? layouts.padding : paddingTop,
          paddingBottom: paddingBottom === true ? layouts.padding : paddingBottom,
          margin: margin === true ? layouts.padding : margin,
          marginHorizontal: marginHorizontal === true ? layouts.padding : marginHorizontal,
          marginVertical: marginVertical === true ? layouts.padding : marginVertical,
          marginLeft: marginLeft === true ? layouts.padding : marginLeft,
          marginRight: marginRight === true ? layouts.padding : marginRight,
          marginTop: marginTop === true ? layouts.padding : marginTop,
          marginBottom: marginBottom === true ? layouts.padding : marginBottom,
        },
        style,
        debug && {
          backgroundColor: 'rgba(255, 0, 0, .5)',
          borderWidth: 1,
          borderColor: 'red',
        },
      ]}
      {...otherProps}
    />
  );
};
