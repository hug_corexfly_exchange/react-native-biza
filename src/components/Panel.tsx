import React, { FC } from 'react';
import { StyleSheet } from 'react-native';

import { View, ViewProps } from './View';
import { useTheme } from '../hooks';

export type PanelProps = ViewProps & {
  edge?: 'none' | 'top' | 'bottom';
  separator?: 'none' | 'top' | 'bottom' | 'both';
};

export const Panel: FC<PanelProps> = ({ edge = 'bottom', separator = 'none', style, ...otherProps }) => {
  const { colors, layouts } = useTheme();
  const builtInStyle = {
    padding: layouts.padding,
    backgroundColor: colors.panel,
    borderColor: colors.edge,
  };

  return <View style={[builtInStyle, edgeStyles[edge], separatorStyles[separator], style]} {...otherProps} />;
};

const edgeStyles = StyleSheet.create({
  none: {},
  top: { borderTopWidth: 1 },
  bottom: { borderBottomWidth: 1 },
});

const SEPARATOR_SIZE = 12;
const separatorStyles = StyleSheet.create({
  none: {},
  top: { marginTop: SEPARATOR_SIZE },
  bottom: { marginBottom: SEPARATOR_SIZE },
  both: { marginVertical: SEPARATOR_SIZE },
});
