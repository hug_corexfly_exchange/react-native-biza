import React, { PropsWithChildren, forwardRef } from 'react';
import { ScrollView, ScrollViewProps, StyleSheet } from 'react-native';

import { withKeyboardAvoidingScroll, KeyboardAvoidingScrollHOCProps } from '../hoc';
import { useTheme } from '../hooks';
import type { ThemeColors, ColorsWithTheme } from '../theme';

type KeyboardAvoidingScrollView = ScrollViewProps & KeyboardAvoidingScrollHOCProps;
const KeyboardAvoidingScrollView = withKeyboardAvoidingScroll(ScrollView);

export type ContentProps = PropsWithChildren<Omit<KeyboardAvoidingScrollView, "scrollEnabled"> & {
  scrollable?: boolean;
  fullScreen?: boolean;
  backgroundColor?: ColorsWithTheme;
}>;

export const Content = forwardRef<ScrollView, ContentProps>(({
  scrollable = false,
  fullScreen = false,
  centerContent = false,
  backgroundColor = 'background',
  contentContainerStyle,
  style,
  ...otherProps
}, ref) => {
  const { colors } = useTheme();

  return (
    <KeyboardAvoidingScrollView
      ref={ref}
      showsVerticalScrollIndicator={false}
      showsHorizontalScrollIndicator={false}
      keyboardShouldPersistTaps="handled"
      scrollEnabled={scrollable}
      contentContainerStyle={[
        contentContainerStyle,
        centerContent ? styles.centerContent : (fullScreen && styles.fullScreen),
      ]}
      style={[
        { backgroundColor: colors[backgroundColor as ThemeColors] || backgroundColor },
        style,
      ]}
      {...otherProps}
    />
  );
});

const styles = StyleSheet.create({
  fullScreen: { flex: 1 },
  centerContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
