import React, { FC } from 'react';
import { StyleSheet } from 'react-native';

import { VisibleAnimation, VisibleAnimationProps } from './VisibleAnimation';
import { View } from './View';
import { useTheme } from '../hooks';

export type ModalProps = VisibleAnimationProps;

export const Modal: FC<ModalProps> = ({
  type = 'slideFromBottom',
  visible,
  onShow,
  onShown,
  onHide,
  onHidden,
  style,
  ...otherProps
}) => {
  const { roundness } = useTheme();

  return (
    <VisibleAnimation
      type={type}
      visible={visible}
      onShow={onShow}
      onShown={onShown}
      onHide={onHide}
      onHidden={onHidden}
      style={styles.animation}
      pointerEvents="box-none"
    >
      <View style={[{ borderRadius: roundness }, styles.container, style]} {...otherProps} />
    </VisibleAnimation>
  );
};

const styles = StyleSheet.create({
  animation: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    left: 0,
    top: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    width: '80%',
    overflow: 'hidden',
  },
});
