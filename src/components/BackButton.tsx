import React, { FC } from 'react';

import { TouchableView, TouchableViewProps } from './TouchableView';
import { useTheme } from '../hooks';
import type { ThemeColors, ColorsWithTheme } from '../theme';
import ArrowLeftSvg from '../assets/arrow-left.svg';

export type BackButtonProps = TouchableViewProps & {
  color?: ColorsWithTheme;
};

export const BackButton: FC<BackButtonProps> = ({ color = 'text', ...otherProps }) => {
  const { colors } = useTheme();

  return (
    <TouchableView {...otherProps}>
      <ArrowLeftSvg stroke={colors[color as ThemeColors] || color} />
    </TouchableView>
  );
}
