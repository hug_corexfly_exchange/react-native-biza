import React, { createContext, FC } from 'react';
import { Text as RNText, TextProps as RNTextProps } from 'react-native';

import { useTheme } from '../hooks';
import type { ThemeFonts, ThemeFontSizes, ThemeFontWeights, ThemeColors, ColorsWithTheme } from '../theme';

export type TextProps = RNTextProps & {
  font?: ThemeFonts;
  weight?: ThemeFontWeights;
  size?: ThemeFontSizes;
  lineHeight?: number;
  color?: ColorsWithTheme;
  align?: 'left' | 'right' | 'center';
  opacity?: number;
};

const TextInHeritContext = createContext({
  font: 'Default',
  weight: 'default',
  size: 'default',
  color: 'text',
  lineHeight: undefined,
} as (
  Required<Pick<TextProps, 'font' | 'weight' | 'size' | 'color'>> &
  Pick<TextProps, 'lineHeight'>
));

export const Text: FC<TextProps> = ({
  font: fontProp,
  weight: weightProp,
  size: sizeProp,
  color: colorProp,
  lineHeight: lineHeightProp,
  align,
  opacity,
  style,
  children,
  ...otherProps
}) => {
  const { colors, fonts } = useTheme();

  return (
    <TextInHeritContext.Consumer>
      {inherit => {
        const font = fontProp || inherit.font
        const weight = weightProp || inherit.weight;
        const size = sizeProp || inherit.size;
        const color = colorProp || inherit.color;

        const fontSizeItem = typeof size === 'string' ?
          fonts[font].size[size] : { fontSize: size, lineHeight: undefined };
        const fontSize = fontSizeItem.fontSize;
        const lineHeight = lineHeightProp || inherit.lineHeight || fontSizeItem.lineHeight;

        return (
          <RNText
            {...otherProps}
            style={[
              {
                ...fonts[font].weight[weight],
                fontSize,
                lineHeight,
                color: colors[color as ThemeColors] || color,
                textAlign: align,
                opacity,
              },
              style,
            ]}
          >
            <TextInHeritContext.Provider value={{ font, weight, size, color, lineHeight }}>
              {children}
            </TextInHeritContext.Provider>
          </RNText>
        );
      }}
    </TextInHeritContext.Consumer>
  );
};
