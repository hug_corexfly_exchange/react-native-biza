import React, { FC } from 'react';

import { Text, TextProps } from './Text';

export type NumberProps = TextProps & {
  value: number;
  locales?: string | string[];
  format?: Intl.NumberFormatOptions;
};

export const Number: FC<NumberProps> = ({ value, locales, format, ...otherProps }) => (
  <Text {...otherProps}>{value.toLocaleString(locales, format)}</Text>
);
