import React, { FC } from 'react';
import { StyleSheet, FlexStyle } from 'react-native';

import { View, ViewProps } from './View';

export type LayerProps = ViewProps & {
  type?: 'auto' | 'underlay' | 'overlay';
  left?: FlexStyle['left'];
  right?: FlexStyle['right'];
  top?: FlexStyle['top'];
  bottom?: FlexStyle['left'];
};

export const Layer: FC<LayerProps> = ({
  type = 'auto',
  left,
  right,
  top,
  bottom,
  pointerEvents = type === 'overlay' ? 'box-none' : 'auto',
  style,
  ...otherProps
}) => {
  let layerStyle = { ...typeStyles[type] };
  if (left != null) layerStyle.left = left;
  if (right != null) layerStyle.right = right;
  if (top != null) layerStyle.top = top;
  if (bottom != null) layerStyle.bottom = bottom;

  return (
    <View
      pointerEvents={pointerEvents}
      style={[styles.layer, layerStyle, style]}
      {...otherProps}
    />
  );
};

const styles = StyleSheet.create({
  layer: { position: 'absolute' },
});

const typeStyles = StyleSheet.create({
  auto: {
    left: 'auto',
    right: 'auto',
    top: 'auto',
    bottom: 'auto',
  },
  underlay: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    zIndex: 0,
  },
  overlay: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
});
