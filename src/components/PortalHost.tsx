import React, { FC, useState, ReactNode } from 'react';

import { PortalContext } from '../contexts';

export type PortalHostProps = {};

export const PortalHost: FC<PortalHostProps> = ({ children }) => {
  const [element, setElement] = useState<ReactNode>();

  return (
    <PortalContext.Provider value={setElement}>
      {children}
      {element}
    </PortalContext.Provider>
  );
};
