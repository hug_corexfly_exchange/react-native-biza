import React, { FC } from 'react';

import { View, ViewProps } from './View';
import { useTheme } from '../hooks';
import type { ThemeColors, ColorsWithTheme } from '../theme';

export type DividerProps = ViewProps & {
  type?: 'horizontal' | 'vertical';
  width?: number;
  size?: number;
  color?: ColorsWithTheme;
  dividerStyle?: ViewProps['style'];
};

export const Divider: FC<DividerProps> = ({
  type = 'horizontal',
  width = 1,
  size,
  color = 'divider',
  dividerStyle,
  style,
  ...otherProps
}) => {
  const { colors } = useTheme();
  const containerStyle = type === 'horizontal' ? {
    width: size ?? '100%',
    height: width,
    flexDirection: 'column' as const,
  } : {
    width,
    height: size ?? '100%',
    flexDirection: 'row' as const,
  };
  let builtInStyle = {
    backgroundColor: colors[color as ThemeColors] || color,
    ...containerStyle,
  };

  return (
    <View style={[containerStyle, style]} {...otherProps}>
      <View style={[builtInStyle, dividerStyle]} />
    </View>
  );
};
