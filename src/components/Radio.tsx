import React, { FC } from 'react';

import { TouchableView, TouchableViewProps } from './TouchableView';
import { useTheme } from '../hooks';
import type { ThemeColors, ColorsWithTheme } from '../theme';
import RadioOnSvg from '../assets/radio-on.svg';
import RadioOffSvg from '../assets/radio-off.svg';

export type RadioProps = TouchableViewProps & {
  checked?: boolean;
  checkedColor?: ColorsWithTheme;
  uncheckedColor?: ColorsWithTheme;
};

export const Radio: FC<RadioProps> = ({
  checked = false,
  checkedColor = 'primary',
  uncheckedColor,
  onPress,
  ...otherProps
}) => {
  const { colors, forms } = useTheme();
  const selectedCheckedColor = colors[checkedColor as ThemeColors] || checkedColor;
  const selectedUncheckedColor = uncheckedColor ? (
    colors[uncheckedColor as ThemeColors] || uncheckedColor
  ) : forms.radio.unchecked;

  return (
    <TouchableView onPress={onPress} {...otherProps}>
      {checked ? (
        <RadioOnSvg stroke={selectedCheckedColor} fill={selectedCheckedColor} />
      ) : (
        <RadioOffSvg stroke={selectedUncheckedColor} />
      )}
    </TouchableView>
  );
};
