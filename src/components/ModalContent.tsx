import React, { FC } from 'react';

import { View, ViewProps } from './View';
import { useTheme } from '../hooks';

export type ModalContentProps = ViewProps;

export const ModalContent: FC<ModalContentProps> = ({ style, ...otherProps }) => {
  const { colors } = useTheme();
  
  return <View style={[{ backgroundColor: colors.white }, style]} {...otherProps} />;
};
