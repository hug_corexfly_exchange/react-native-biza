import React, { FC } from 'react';
import { StyleSheet } from 'react-native';

import { View, ViewProps } from './View';
import { useTheme } from '../hooks';

export type FooterBarProps = ViewProps;

export const FooterBar: FC<FooterBarProps> = ({ style, ...otherProps }) => {
  const { layouts } = useTheme();

  return <View style={[{ minHeight: layouts.footer }, styles.wrapper, style]} {...otherProps} />;
};

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
