import React, { FC, useEffect, useRef } from 'react';
import { Animated, Easing, ViewProps, StyleSheet } from 'react-native';

import { useTheme } from '../hooks';
import type { ThemeColors, ColorsWithTheme } from '../theme';
import LoadingSvg from '../assets/loading.svg';

export type LoadingProps = ViewProps & {
  size?: 'small' | 'medium' | 'large';
  color?: ColorsWithTheme;
  style?: Animated.WithAnimatedValue<ViewProps['style']>;
};

export const Loading: FC<LoadingProps> = ({ size = 'medium', color = 'primary', style, ...otherProps }) => {
  const rotation = useRef(new Animated.Value(0)).current;
  useEffect(() => {
    rotation.setValue(0);
    Animated.loop(
      Animated.timing(rotation, {
        toValue: 1,
        duration: 1000,
        easing: Easing.linear,
        useNativeDriver: true,
      })
    ).start();
  }, []);

  const { colors } = useTheme();
  const selectedColor = colors[color as ThemeColors] || colors;

  return (
    <Animated.View
      style={[
        {
          transform: [{
            rotateZ: rotation.interpolate({
              inputRange: [0, 1],
              outputRange: ['0deg', '360deg'],
            }),
          }],
        },
        sizeStyles[size],
        style,
      ]}
      {...otherProps}
    >
      <LoadingSvg width="100%" height="100%" stroke={selectedColor} />
    </Animated.View>
  );
};

const SMALL_SIZE = 20;
const MEDIUM_SIZE = 30;
const LARGE_SIZE = 40;
const sizeStyles = StyleSheet.create({
  small: {
    width: SMALL_SIZE,
    height: SMALL_SIZE,
  },
  medium: {
    width: MEDIUM_SIZE,
    height: MEDIUM_SIZE,
  },
  large: {
    width: LARGE_SIZE,
    height: LARGE_SIZE,
  },
});
