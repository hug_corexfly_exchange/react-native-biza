import React, { FC } from 'react';
import { AlertButton as RNAlertButton, StyleSheet } from 'react-native';

import { Button, ButtonProps } from './Button';
import { Text } from './Text';
import { ColorsWithTheme, orange8 } from '../theme';

export type ModalButtonProps = Omit<ButtonProps, 'type'> & {
  type?: RNAlertButton['style'];
};

export const ModalButton: FC<ModalButtonProps> = ({ type = 'default', style, children, ...otherProps }) => {
  let buttonColor: ColorsWithTheme = 'primary';
  let textColor: ColorsWithTheme = 'white';
  if (type === 'cancel') {
    buttonColor = 'white'
    textColor = 'primary';
  } else if (type === 'destructive') {
    buttonColor = 'white'
    textColor = orange8;
  }

  return (
    <Button size="regular" color={buttonColor} rounded={0} style={[styles.button, style]} {...otherProps}>
      <Text color={textColor}>{children}</Text>
    </Button>
  );
};

const styles = StyleSheet.create({
  button: { flex: 1 },
});
