import React, { FC, useMemo } from 'react';

import { Modal } from './Modal';
import { ModalHeader } from './ModalHeader';
import { ModalContent } from './ModalContent';
import { ModalButtons } from './ModalButtons';
import { ModalButton } from './ModalButton';
import { Layout } from './Layout';
import { Text } from './Text';
import { ModalControllerContext } from '../contexts';
import { useModalManager } from '../hooks';
import { Backdrop } from './Backdrop';

export const ModalProvider: FC = ({ children }) => {
  const manager = useModalManager();
  const context = useMemo(() => ({
    present: manager.present,
    show: manager.show,
    alert: manager.alert,
    hasAnyAppeared: manager.hasAnyAppeared,
    dismiss: manager.dismiss,
  }), []);

  return (
    <ModalControllerContext.Provider value={context}>
      {children}
      <Backdrop visible={manager.hasVisibledModal} />
      {manager.render(({ id, visible, title, closeable, content: Content, buttons }) => (
        typeof Content === 'function' ? (
          // @ts-ignore
          <Content id={id} visible={visible} onHidden={() => manager.dispose(id)} key={id} />
        ) : (
          <Modal visible={visible} onHidden={() => manager.dispose(id)} key={id}>
            {title && (
              <ModalHeader closeable={closeable} onPressClose={() => manager.dismiss(id)}>
                {title}
              </ModalHeader>
            )}
            <ModalContent>
              <Layout padding marginVertical>
                <Text align="center">{Content}</Text>
              </Layout>
            </ModalContent>
            {buttons.length > 0 && (
              <ModalButtons>
                {buttons.map(({ text, onPress, style }, index) => (
                  <ModalButton
                    type={style}
                    onPress={() => {
                      if (!onPress?.()) manager.dismiss(id);
                    }}
                    key={index}
                  >
                    {text || 'OK'}
                  </ModalButton>
                ))}
              </ModalButtons>
            )}
          </Modal>
        )
      ))}
    </ModalControllerContext.Provider>
  );
};
