import React, { FC, useEffect, useRef } from 'react';
import { Animated, Easing } from 'react-native';

import { Wrapper, WrapperProps } from './Wrapper';
import { useTheme } from '../hooks';

export type SkeletonProps = WrapperProps & {
  highlightOpacity?: number;
  style?: Animated.WithAnimatedValue<WrapperProps['style']>;
};

export const Skeleton: FC<SkeletonProps> = ({
  highlightOpacity = 0.6,
  color = 'skeleton',
  style,
  ...otherProps
}) => {
  const { animation } = useTheme();
  const opacity = useRef(new Animated.Value(1)).current;
  useEffect(() => {
    opacity.setValue(1);
    Animated.loop(
      Animated.sequence([
        Animated.timing(opacity, {
          toValue: highlightOpacity,
          duration: animation,
          delay: animation,
          easing: Easing.inOut(Easing.ease),
          useNativeDriver: true,
        }),
        Animated.timing(opacity, {
          toValue: 1,
          duration: animation,
          easing: Easing.inOut(Easing.ease),
          useNativeDriver: true,
        }),
        Animated.timing(opacity, {
          toValue: 1,
          duration: 1000,
          easing: Easing.inOut(Easing.ease),
          useNativeDriver: true,
        }),
      ])
    ).start();
  }, [highlightOpacity, animation]);

  return (
    <Wrapper
      {...otherProps}
      animatable
      color={color}
      style={[{ opacity }, style]}
    />
  );
};
