import React, { FC } from 'react';
import { View, ViewProps, StyleSheet } from 'react-native';

import { SafeAreaView } from './SafeAreaView';

export type HeaderProps = ViewProps & {
  safeArea?: 'auto' | 'always';
};

export const Header: FC<HeaderProps> = ({ safeArea = 'auto', style, ...otherProps }) => safeArea ? (
  <SafeAreaView
    inset={safeArea === 'auto' ? 'auto' : 'top'}
    style={[styles.order, style]}
    {...otherProps}
  />
) : (
  <View {...otherProps} />
);

const styles = StyleSheet.create({
  order: {
    zIndex: 9,
  },
});
