import React, { FC } from 'react';
import { StyleSheet } from 'react-native';

import { View, ViewProps } from './View';
import { Text } from './Text';
import { TouchableView } from './TouchableView';
import { useTheme } from '../hooks';
import ModalCloseSvg from '../assets/modal-close.svg';

export type ModalHeaderProps = ViewProps & {
  closeable?: boolean;
  onPressClose?: () => void;
};

export const ModalHeader: FC<ModalHeaderProps> = ({
  closeable = true,
  onPressClose,
  children,
  style,
  ...otherProps
}) => {
  const { layouts, colors } = useTheme();

  return (
    <View
      style={[
        {
          padding: layouts.padding,
          backgroundColor: colors.white,
          borderColor: colors.edge,
        },
        styles.wrapper,
        style,
      ]}
      {...otherProps}
    >
      <Text weight="bold" size="level3" style={styles.title}>{children}</Text>
      {closeable && (
        <TouchableView onPress={onPressClose} style={{ paddingLeft: layouts.padding }}>
          <ModalCloseSvg />
        </TouchableView>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
  },
  title: { flex: 1 },
});
