import { useState, useMemo, ReactNode, useEffect, ComponentType, useRef } from 'react';
import type { AlertButton } from 'react-native';

type ModalContentProps = {
  id: number;
  visible: boolean;
};

class ModalItem {
  public timestamp: number;
  public id: number;
  public visible: boolean;
  public delay: number;
  public title?: ReactNode;
  public closeable: boolean;
  public content: ReactNode | ComponentType<ModalContentProps>;
  public buttons: AlertButton[];
  public onDismiss?: () => void;
  public appearedAt?: number;
  public disappearedAt?: number;

  public constructor(
    options: Required<Pick<ModalItem, 'content'>> &
      Partial<Pick<ModalItem, 'timestamp' | 'id' | 'visible' | 'delay' | 'title' | 'closeable' | 'buttons' | 'onDismiss' | 'appearedAt' | 'disappearedAt'>>
  ) {
    this.timestamp = options.timestamp || Date.now();
    this.id = options.id || this.timestamp;
    this.visible = options.visible || false;
    this.delay = options.delay || 0;
    this.title = options.title;
    this.closeable = options.closeable || false;
    this.content = options.content;
    this.buttons = options.buttons || [{ text: 'OK', style: 'default' }];
    this.onDismiss = options.onDismiss;
    this.appearedAt = options.appearedAt;
    this.disappearedAt = options.disappearedAt;
  }

  public appear() {
    this.visible = true;
    this.appearedAt = Date.now();
  }

  public disappear() {
    this.visible = false;
    this.disappearedAt = Date.now();
    this.onDismiss?.();
  }

  public isAppeared() {
    return this.appearedAt && !this.disappearedAt;
  }

  public isDisappeared() {
    return this.disappearedAt;
  }
}

type ItemAddOptions = Partial<Pick<ModalItem, 'delay'>>;
type ModalAddOptions = Partial<Pick<ModalItem, 'delay' | 'title' | 'closeable' | 'buttons'>>;

function isDuplicatedId(id: number, collections: ModalItem[]) {
  if (collections.length === 0) return false;
  let i = collections.length - 1;
  while (i >= 0 && collections[i].id !== id) i--;
  return i >= 0;
};

function findLastIndexOfAppeared(modals: ModalItem[]) {
  let i = modals.length - 1;
  while (i >= 0 && modals[i].isDisappeared()) i--;
  return i;
}

export function useModalManager() {
  const [modals, setModals] = useState<ModalItem[]>([]);
  const modalsRef = useRef(modals);
  modalsRef.current = modals;

  useEffect(() => {
    const timeouts = modals.map((alert, index) => {
      let next = [...modals];
      if (!alert.appearedAt) {
        return setTimeout(() => {
          next[index] = new ModalItem(alert);
          next[index].appear();
          setModals(next);
        }, alert.timestamp + alert.delay - Date.now());
      }

      return null;
    });

    return () => {
      timeouts.forEach(timeout => {
        if (timeout) clearTimeout(timeout);
      });
    };
  }, [modals]);

  return useMemo(() => ({
    present(content: ComponentType<ModalContentProps>, options: ItemAddOptions = {}) {
      setModals(previous => {
        let next = previous.map(alert => {
          let newItem = new ModalItem(alert);
          newItem.disappear();
          return newItem;
        });
        let newItem = new ModalItem({ content, ...options });
        if (isDuplicatedId(newItem.id, next)) newItem.id += 1;
        next.push(newItem);
        return next;
      });
    },
    show(content: ComponentType<ModalContentProps>, options: ItemAddOptions = {}) {
      setModals(previous => {
        let next = [...previous];
        let newItem = new ModalItem({ content, ...options });
        if (isDuplicatedId(newItem.id, next)) newItem.id += 1;
        next.push(newItem);
        return next;
      });
    },
    alert(content: ReactNode, options: ModalAddOptions = {}) {
      setModals(previous => {
        let next = [...previous];
        let newItem = new ModalItem({ content, ...options });
        if (isDuplicatedId(newItem.id, next)) newItem.id += 1;
        next.push(newItem);
        return next;
      });
    },
    hasAnyAppeared() {
      return findLastIndexOfAppeared(modalsRef.current) !== -1;
    },
    dismiss(id?: number) {
      setModals(previous => {
        let next = [...previous];
        if (id != null) {
          const findedIndex = next.findIndex(item => item.id === id);
          if (findedIndex !== -1) {
            next[findedIndex] = new ModalItem(next[findedIndex]);
            next[findedIndex].disappear();
          }
        } else {
          const index = findLastIndexOfAppeared(next);
          if (index !== -1) next[index].disappear();
        }
        return next;
      });
    },
    dispose(id: number) {
      setModals(previous => {
        let next = [...previous];
        const findedIndex = next.findIndex(item => item.id === id);
        if (findedIndex !== -1) next.splice(findedIndex, 1);
        return next;
      });
    },
    render(callback: (alert: ModalItem, index: number) => ReactNode) {
      return modals.map(callback);
    },
    hasVisibledModal: modals.filter(alert => alert.visible).length > 0,
  }), [modals]);
}
