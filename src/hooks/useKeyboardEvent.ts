import React from 'react';
import { Keyboard, KeyboardEvent } from 'react-native';

export type KeyboardEventHookOptions = {
  onWillShow?: (event: KeyboardEvent) => void;
  onDidShow?: (event: KeyboardEvent) => void;
  onWillHide?: (event: KeyboardEvent) => void;
  onDidHide?: (event: KeyboardEvent) => void; 
} | {
  [key: string]: (event: KeyboardEvent) => void;
};

export function useKeyboardEvent(options: KeyboardEventHookOptions = {}) {
  const { onWillShow, onDidShow, onWillHide, onDidHide } = options;
  React.useEffect(() => {
    const subscriptions = [
      Keyboard.addListener(`keyboardWillShow`, event => onWillShow?.(event)),
      Keyboard.addListener(`keyboardDidShow`, event => onDidShow?.(event)),
      Keyboard.addListener(`keyboardWillHide`, event => onWillHide?.(event)),
      Keyboard.addListener(`keyboardDidHide`, event => onDidHide?.(event)),
    ];

    return () => {
      subscriptions.forEach(subscription => subscription.remove());
    };
  });
}
