import { useState, useMemo, ReactNode, useEffect, ComponentType } from 'react';

import { useTheme } from './useTheme';

type MessageProps = {
  id: number;
  visible: boolean;
};

class ToastItem {
  public timestamp: number;
  public id: number;
  public visible: boolean;
  public delay: number;
  public duration: number;
  public message: ReactNode | ComponentType<MessageProps>;
  public action?: string;
  public onAction?: () => void;
  public onDismiss?: () => void;
  public appearedAt?: number;
  public disappearedAt?: number;

  public constructor(
    options: Required<Pick<ToastItem, 'message' | 'duration'>> &
      Partial<Pick<ToastItem, 'timestamp' | 'id' | 'visible' | 'delay' | 'action' | 'onAction' | 'onDismiss' | 'appearedAt' | 'disappearedAt'>>
  ) {
    this.timestamp = options.timestamp || Date.now();
    this.id = options.id || this.timestamp;
    this.visible = options.visible || false;
    this.delay = options.delay || 0;
    this.duration = options.duration;
    this.message = options.message;
    this.action = options.action;
    this.onAction = options.onAction;
    this.onDismiss = options.onDismiss;
    this.appearedAt = options.appearedAt;
    this.disappearedAt = options.disappearedAt;
  }

  public appear() {
    this.visible = true;
    this.appearedAt = Date.now();
  }

  public disappear() {
    this.visible = false;
    this.disappearedAt = Date.now();
    this.onDismiss?.();
  }
}

type ItemAddOptions = Partial<Pick<ToastItem, 'delay' | 'duration'>>;
type AlertAddOptions = Partial<Pick<ToastItem, 'delay' | 'duration' | 'action' | 'onAction'>>;

export function useToastManager(duration?: number) {
  const { toast } = useTheme();
  const [toasts, setToasts] = useState<ToastItem[]>([]);

  useEffect(() => {
    const timeouts = toasts.map((toast, index) => {
      let next = [...toasts];
      if (!toast.appearedAt) {
        return setTimeout(() => {
          next[index] = new ToastItem(toast);
          next[index].appear();
          setToasts(next);
        }, toast.timestamp + toast.delay - Date.now());
      } else if (!toast.disappearedAt) {
        return setTimeout(() => {
          next[index] = new ToastItem(toast);
          next[index].disappear();
          setToasts(next);
        }, toast.appearedAt + toast.duration - Date.now());
      }

      return null;
    });

    return () => {
      timeouts.forEach(timeout => {
        if (timeout) clearTimeout(timeout);
      });
    };
  }, [toasts]);

  const isDuplicatedId = (id: number, collections: ToastItem[]) => {
    if (collections.length === 0) return false;
    let i = collections.length - 1;
    while (i >= 0 && collections[i].id !== id) --i;
    return i >= 0;
  };

  return useMemo(() => ({
    present(message: ComponentType<MessageProps>, options: ItemAddOptions = {}) {
      setToasts(previous => {
        let next = previous.map(toast => {
          let newItem = new ToastItem(toast);
          newItem.disappear();
          return newItem;
        });
        let newItem = new ToastItem({
          message,
          ...options,
          duration: options.duration || duration || toast.duration,
        });
        if (isDuplicatedId(newItem.id, next)) newItem.id += 1;
        next.push(newItem);
        return next;
      });
    },
    show(message: ComponentType<MessageProps>, options: ItemAddOptions = {}) {
      setToasts(previous => {
        let next = [...previous];
        let newItem = new ToastItem({
          message,
          ...options,
          duration: options.duration || duration || toast.duration,
        });
        if (isDuplicatedId(newItem.id, next)) newItem.id += 1;
        next.push(newItem);
        return next;
      });
    },
    alert(message: ReactNode, options: AlertAddOptions = {}) {
      setToasts(previous => {
        let next = [...previous];
        let newItem = new ToastItem({
          message,
          ...options,
          duration: options.duration || duration || toast.duration,
        });
        if (isDuplicatedId(newItem.id, next)) newItem.id += 1;
        next.push(newItem);
        return next;
      });
    },
    dismiss(id: number) {
      setToasts(previous => {
        let next = [...previous];
        const findedIndex = next.findIndex(item => item.id === id);
        if (findedIndex !== -1) {
          next[findedIndex] = new ToastItem(next[findedIndex]);
          next[findedIndex].disappear();
        }
        return next;
      });
    },
    dispose(id: number) {
      setToasts(previous => {
        let next = [...previous];
        const findedIndex = next.findIndex(item => item.id === id);
        if (findedIndex !== -1) next.splice(findedIndex, 1);
        return next;
      });
    },
    render(callback: (toast: ToastItem, index: number) => ReactNode) {
      return toasts.map(callback);
    },
  }), [toasts]);
}
