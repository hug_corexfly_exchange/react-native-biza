import React from 'react';
import { Platform } from 'react-native';

import { useKeyboardEvent } from './useKeyboardEvent';

type KeyboardEventTriggerTimePoint =  'Will' | 'Did';
export type KeyboardHeightHookOptions = {
  when?: KeyboardEventTriggerTimePoint;
  whenShow?: KeyboardEventTriggerTimePoint;
  whenHide?: KeyboardEventTriggerTimePoint;
};

export function useKeyboardHeight(options: KeyboardHeightHookOptions = {}) {
  const {
    when = Platform.select({ ios: 'Will', android: 'Did' }),
    whenShow = when,
    whenHide = when,
  } = options;

  const [keyboardHeight, setKeyboardHeight] = React.useState(0);
  useKeyboardEvent({
    [`on${whenShow}Show`]: event => setKeyboardHeight(event.endCoordinates.height),
    [`on${whenHide}Hide`]: () => setKeyboardHeight(0),
  });

  return keyboardHeight;
}
