import { useContext } from 'react';

import { ModalControllerContext } from '../contexts';

export function useModalController() {
  return useContext(ModalControllerContext);
}
