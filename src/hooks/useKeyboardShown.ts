import React from 'react';
import { Platform } from 'react-native';

import { useKeyboardEvent } from './useKeyboardEvent';

type KeyboardEventTriggerTimePoint =  'Will' | 'Did';
export type KeyboardShownHookOptions = {
  when?: KeyboardEventTriggerTimePoint;
  whenShow?: KeyboardEventTriggerTimePoint;
  whenHide?: KeyboardEventTriggerTimePoint;
};

export function useKeyboardShown(options: KeyboardShownHookOptions = {}) {
  const {
    when = Platform.select({ ios: 'Will', android: 'Did' }),
    whenShow = when,
    whenHide = when,
  } = options;

  const [shown, setShown] = React.useState(false);
  useKeyboardEvent({
    [`on${whenShow}Show`]: () => setShown(true),
    [`on${whenHide}Hide`]: () => setShown(false),
  });
  
  return shown;
}
