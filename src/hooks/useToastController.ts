import { useContext } from 'react';

import { ToastControllerContext } from '../contexts';

export function useToastController() {
  return useContext(ToastControllerContext);
}
