import {
  white,
  black,
  gray0,
  gray1,
  gray2,
  gray3,
  gray4,
  gray5,
  gray6,
  gray9,
  red7,
  grape5,
  teal5,
  yellow5,
  yellow7,
  orange8,
  primary4,
} from './Colors';
import { configureFonts } from './Fonts';

export const DefaultTheme = {
  roundness: 6,
  layouts: {
    header: 50,
    footer: 50,
    padding: 16,
  },
  colors: {
    primary: primary4,
    accent: grape5,
    white,
    black,
    success: teal5,
    danger: red7,
    warning: yellow5,
    caution: orange8,
    background: gray1,
    placeholder: gray5,
    notification: yellow7,
    divider: gray3,
    border: gray4,
    surface: white,
    panel: white,
    edge: 'rgba(0, 0, 0, 0.1)' as UnsafetyRGBAColorType,
    header: white,
    footer: white,
    text: gray9,
    backdrop: 'rgba(0, 0, 0, .3)' as UnsafetyRGBAColorType,
    skeleton: gray3,
  },
  forms: {
    button: {
      font: {
        wegith: 'bold' as const,
      },
      muted: {
        background: gray2,
        text: gray4,
      },
    },
    input: {
      muted: {
        background: gray0,
        text: gray6,
      },
    },
    checkbox: {
      unchecked: '#DADADA' as UnsafetyHexColorType,
    },
    radio: {
      unchecked: gray6,
    },
  },
  gradients: {
    background: ['#8E54E9', '#4776E6'],
    button: ['#9C36B5', '#4776E6'],
  } as Record<string, (UnsafetyHexColorType | UnsafetyRGBAColorType)[]>,
  fonts: configureFonts(),
  animation: 200,
  toast: {
    background: '#454545' as UnsafetyRGBAColorType,
    color: 'white' as UnsafetyRGBAColorType,
    duration: 3000,
  },
};

export type Theme = typeof DefaultTheme;
export type ThemeFonts = keyof Theme['fonts'];
export type ThemeFontSizes = keyof Theme['fonts'][0]['size'];
export type ThemeFontWeights = keyof Theme['fonts'][0]['weight'];
export type ThemeColors = keyof Theme['colors'];
export type ColorsWithTheme = ThemeColors | UnsafetyHexColorType | UnsafetyRGBAColorType | ReservedColorNames;
