import { StyleSheet, TextStyle } from 'react-native';

type FontWeight = (
  'black' | 'extraBold' | 'bold' | 'semiBold' | 'medium' | 'regular' | 'light' | 'extraLight' | 'thin' | 'default'
);
type FontWeightMap = {
  [name in FontWeight]: Required<Pick<TextStyle, 'fontFamily' | 'fontWeight'>>;
};

const DefaultFontWeights = StyleSheet.create({
  black: {
    fontFamily: 'System',
    fontWeight: '900'
  },
  extraBold: {
    fontFamily: 'System',
    fontWeight: '800',
  },
  bold: {
    fontFamily: 'System',
    fontWeight: '700',
  },
  semiBold: {
    fontFamily: 'System',
    fontWeight: '600',
  },
  medium: {
    fontFamily: 'System',
    fontWeight: '500',
  },
  regular: {
    fontFamily: 'System',
    fontWeight: '400',
  },
  light: {
    fontFamily: 'System',
    fontWeight: '300',
  },
  extraLight: {
    fontFamily: 'System',
    fontWeight: '200',
  },
  thin: {
    fontFamily: 'System',
    fontWeight: '100',
  },
  default: {
    fontFamily: 'System',
    fontWeight: '400',
  },
} as FontWeightMap);

export function configureFontWeights(overrides: Partial<FontWeightMap> = {}) {
  return StyleSheet.create({
    ...DefaultFontWeights,
    ...overrides,
  });
}

type FontSize = (
  'level1' | 'level2' | 'level3' | 'level4' | 'level5' | 'level6' | 'level7' | 'default'
);
type FontSizeMap = {
  [name in FontSize]: Required<Pick<TextStyle, 'fontSize'>> & Pick<TextStyle, 'lineHeight'>;
};

const DefaultFontSizes = StyleSheet.create({
  level1: {
    fontSize: 30,
  },
  level2: {
    fontSize: 23,
  },
  level3: {
    fontSize: 19,
  },
  level4: {
    fontSize: 16,
  },
  level5: {
    fontSize: 14,
  },
  level6: {
    fontSize: 12,
  },
  level7: {
    fontSize: 10,
  },
  default: {
    fontSize: 16,
  },
} as FontSizeMap);

export function configureFontSizes(overrides: Partial<FontSizeMap> = {}) {
  return StyleSheet.create({
    ...DefaultFontSizes,
    ...overrides,
  });
}

type Font = {
  weight: FontWeightMap;
  size: FontSizeMap;
};
type FontMap = Record<string, Font>;

export function configureFonts(overrides: FontMap = {}): FontMap {
  const systemFont = {
    weight: configureFontWeights(),
    size: configureFontSizes(),
  };

  return {
    'System': systemFont,
    'Default': systemFont,
    ...overrides,
  };
}
