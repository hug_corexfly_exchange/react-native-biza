/**
 * Open Color
 * 
 * Open color is an open-source color scheme optimized for UI
 * like font, background, border, etc.
 * 
 * version: 1.7.0
 * github: https://github.com/yeun/open-color
 */

/* General */
export const white: UnsafetyHexColorType = '#ffffff';
export const black: UnsafetyHexColorType = '#000000';

/* Gray */
export const gray0: UnsafetyHexColorType = '#f8f9fa';
export const gray1: UnsafetyHexColorType = '#f1f3f5';
export const gray2: UnsafetyHexColorType = '#e9ecef';
export const gray3: UnsafetyHexColorType = '#dee2e6';
export const gray4: UnsafetyHexColorType = '#ced4da';
export const gray5: UnsafetyHexColorType = '#adb5bd';
export const gray6: UnsafetyHexColorType = '#868e96';
export const gray7: UnsafetyHexColorType = '#495057';
export const gray8: UnsafetyHexColorType = '#343a40';
export const gray9: UnsafetyHexColorType = '#212529';

/* Red */
export const red0: UnsafetyHexColorType = '#fff5f5';
export const red1: UnsafetyHexColorType = '#ffe3e3';
export const red2: UnsafetyHexColorType = '#ffc9c9';
export const red3: UnsafetyHexColorType = '#ffa8a8';
export const red4: UnsafetyHexColorType = '#ff8787';
export const red5: UnsafetyHexColorType = '#ff6b6b';
export const red6: UnsafetyHexColorType = '#fa5252';
export const red7: UnsafetyHexColorType = '#f03e3e';
export const red8: UnsafetyHexColorType = '#e03131';
export const red9: UnsafetyHexColorType = '#c92a2a';

/* Pink */
export const pink0: UnsafetyHexColorType = '#fff0f6';
export const pink1: UnsafetyHexColorType = '#ffdeeb';
export const pink2: UnsafetyHexColorType = '#fcc2d7';
export const pink3: UnsafetyHexColorType = '#faa2c1';
export const pink4: UnsafetyHexColorType = '#f783ac';
export const pink5: UnsafetyHexColorType = '#f06595';
export const pink6: UnsafetyHexColorType = '#e64980';
export const pink7: UnsafetyHexColorType = '#d6336c';
export const pink8: UnsafetyHexColorType = '#c2255c';
export const pink9: UnsafetyHexColorType = '#a61e4d';

/* Grape */
export const grape0: UnsafetyHexColorType = '#f8f0fc';
export const grape1: UnsafetyHexColorType = '#f3d9fa';
export const grape2: UnsafetyHexColorType = '#eebefa';
export const grape3: UnsafetyHexColorType = '#e599f7';
export const grape4: UnsafetyHexColorType = '#da77f2';
export const grape5: UnsafetyHexColorType = '#cc5de8';
export const grape6: UnsafetyHexColorType = '#be4bdb';
export const grape7: UnsafetyHexColorType = '#ae3ec9';
export const grape8: UnsafetyHexColorType = '#9c36b5';
export const grape9: UnsafetyHexColorType = '#862e9c';

/* Violet */
export const violet0: UnsafetyHexColorType = '#f3f0ff';
export const violet1: UnsafetyHexColorType = '#e5dbff';
export const violet2: UnsafetyHexColorType = '#d0bfff';
export const violet3: UnsafetyHexColorType = '#b197fc';
export const violet4: UnsafetyHexColorType = '#9775fa';
export const violet5: UnsafetyHexColorType = '#845ef7';
export const violet6: UnsafetyHexColorType = '#7950f2';
export const violet7: UnsafetyHexColorType = '#7048e8';
export const violet8: UnsafetyHexColorType = '#6741d9';
export const violet9: UnsafetyHexColorType = '#5f3dc4';

/* Indigo */
export const indigo0: UnsafetyHexColorType = '#edf2ff';
export const indigo1: UnsafetyHexColorType = '#dbe4ff';
export const indigo2: UnsafetyHexColorType = '#bac8ff';
export const indigo3: UnsafetyHexColorType = '#91a7ff';
export const indigo4: UnsafetyHexColorType = '#748ffc';
export const indigo5: UnsafetyHexColorType = '#5c7cfa';
export const indigo6: UnsafetyHexColorType = '#4c6ef5';
export const indigo7: UnsafetyHexColorType = '#4263eb';
export const indigo8: UnsafetyHexColorType = '#3b5bdb';
export const indigo9: UnsafetyHexColorType = '#364fc7';

/* Blue */
export const blue0: UnsafetyHexColorType = '#e7f5ff';
export const blue1: UnsafetyHexColorType = '#d0ebff';
export const blue2: UnsafetyHexColorType = '#a5d8ff';
export const blue3: UnsafetyHexColorType = '#74c0fc';
export const blue4: UnsafetyHexColorType = '#4dabf7';
export const blue5: UnsafetyHexColorType = '#339af0';
export const blue6: UnsafetyHexColorType = '#228be6';
export const blue7: UnsafetyHexColorType = '#1c7ed6';
export const blue8: UnsafetyHexColorType = '#1971c2';
export const blue9: UnsafetyHexColorType = '#1864ab';

/* Cyan */
export const cyan0: UnsafetyHexColorType = '#e3fafc';
export const cyan1: UnsafetyHexColorType = '#c5f6fa';
export const cyan2: UnsafetyHexColorType = '#99e9f2';
export const cyan3: UnsafetyHexColorType = '#66d9e8';
export const cyan4: UnsafetyHexColorType = '#3bc9db';
export const cyan5: UnsafetyHexColorType = '#22b8cf';
export const cyan6: UnsafetyHexColorType = '#15aabf';
export const cyan7: UnsafetyHexColorType = '#1098ad';
export const cyan8: UnsafetyHexColorType = '#0c8599';
export const cyan9: UnsafetyHexColorType = '#0b7285';

/* Teal */
export const teal0: UnsafetyHexColorType = '#e6fcf5';
export const teal1: UnsafetyHexColorType = '#c3fae8';
export const teal2: UnsafetyHexColorType = '#96f2d7';
export const teal3: UnsafetyHexColorType = '#63e6be';
export const teal4: UnsafetyHexColorType = '#38d9a9';
export const teal5: UnsafetyHexColorType = '#20c997';
export const teal6: UnsafetyHexColorType = '#12b886';
export const teal7: UnsafetyHexColorType = '#0ca678';
export const teal8: UnsafetyHexColorType = '#099268';
export const teal9: UnsafetyHexColorType = '#087f5b';

/* Green */
export const green0: UnsafetyHexColorType = '#ebfbee';
export const green1: UnsafetyHexColorType = '#d3f9d8';
export const green2: UnsafetyHexColorType = '#b2f2bb';
export const green3: UnsafetyHexColorType = '#8ce99a';
export const green4: UnsafetyHexColorType = '#69db7c';
export const green5: UnsafetyHexColorType = '#51cf66';
export const green6: UnsafetyHexColorType = '#40c057';
export const green7: UnsafetyHexColorType = '#37b24d';
export const green8: UnsafetyHexColorType = '#2f9e44';
export const green9: UnsafetyHexColorType = '#2b8a3e';

/* Lime */
export const lime0: UnsafetyHexColorType = '#f4fce3';
export const lime1: UnsafetyHexColorType = '#e9fac8';
export const lime2: UnsafetyHexColorType = '#d8f5a2';
export const lime3: UnsafetyHexColorType = '#c0eb75';
export const lime4: UnsafetyHexColorType = '#a9e34b';
export const lime5: UnsafetyHexColorType = '#94d82d';
export const lime6: UnsafetyHexColorType = '#82c91e';
export const lime7: UnsafetyHexColorType = '#74b816';
export const lime8: UnsafetyHexColorType = '#66a80f';
export const lime9: UnsafetyHexColorType = '#5c940d';

/* Yellow */
export const yellow0: UnsafetyHexColorType = '#fff9db';
export const yellow1: UnsafetyHexColorType = '#fff3bf';
export const yellow2: UnsafetyHexColorType = '#ffec99';
export const yellow3: UnsafetyHexColorType = '#ffe066';
export const yellow4: UnsafetyHexColorType = '#ffd43b';
export const yellow5: UnsafetyHexColorType = '#fcc419';
export const yellow6: UnsafetyHexColorType = '#fab005';
export const yellow7: UnsafetyHexColorType = '#f59f00';
export const yellow8: UnsafetyHexColorType = '#f08c00';
export const yellow9: UnsafetyHexColorType = '#e67700';

/* Orange */
export const orange0: UnsafetyHexColorType = '#fff4e6';
export const orange1: UnsafetyHexColorType = '#ffe8cc';
export const orange2: UnsafetyHexColorType = '#ffd8a8';
export const orange3: UnsafetyHexColorType = '#ffc078';
export const orange4: UnsafetyHexColorType = '#ffa94d';
export const orange5: UnsafetyHexColorType = '#ff922b';
export const orange6: UnsafetyHexColorType = '#fd7e14';
export const orange7: UnsafetyHexColorType = '#f76707';
export const orange8: UnsafetyHexColorType = '#e8590c';
export const orange9: UnsafetyHexColorType = '#d9480f';
