export * from './OpenColors';

export const primary1: UnsafetyHexColorType = '#CFDFFF';
export const primary2: UnsafetyHexColorType = '#9FBFFF';
export const primary3: UnsafetyHexColorType = '#6F9FFF';
export const primary4: UnsafetyHexColorType = '#3F7FFF';
export const primary5: UnsafetyHexColorType = '#0F5FFF';
export const primary6: UnsafetyHexColorType = '#0C4CCC';
