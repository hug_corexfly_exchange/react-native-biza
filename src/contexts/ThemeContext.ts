import { createContext } from 'react';

import { DefaultTheme } from '../theme';

export const ThemeContext = createContext(DefaultTheme);
