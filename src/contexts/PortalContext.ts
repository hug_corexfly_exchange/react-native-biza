import { createContext, Dispatch, ReactNode, SetStateAction } from 'react';

export type PortalContextType = Dispatch<SetStateAction<ReactNode>>;

export const PortalContext = createContext((() => {}) as PortalContextType);
