import { createContext } from 'react';

import type { useToastManager } from '../hooks/useToastManager';

export type ToastControllerContextType = Pick<ReturnType<typeof useToastManager>, 'present' | 'show' | 'alert' | 'dismiss'>;

export const ToastControllerContext = createContext({} as ToastControllerContextType);
