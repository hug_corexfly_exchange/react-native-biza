import { createContext } from 'react';

import type { useModalManager } from '../hooks/useModalManager';

export type ModalControllerContextType = Pick<ReturnType<typeof useModalManager>, 'present' | 'show' | 'alert' | 'hasAnyAppeared' | 'dismiss'>;

export const ModalControllerContext = createContext({} as ModalControllerContextType);
