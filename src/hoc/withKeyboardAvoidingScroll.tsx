import React, { ComponentClass, Component, forwardRef, useRef, useState } from 'react';
import {
  Platform,
  TextInput,
  ScrollViewProps,
  ScrollView,
  FlatList,
  SectionList,
  VirtualizedList,
  UIManager,
  findNodeHandle,
  Dimensions,
  KeyboardEvent,
  LayoutAnimation,
} from 'react-native';

import { useKeyboardEvent } from '../hooks';

type ScrollableComponentClass = (
  typeof ScrollView |
  typeof VirtualizedList |
  typeof FlatList |
  typeof SectionList
);

type MeasuerInWindow = {
  x: number;
  y: number;
  width: number;
  height: number;
};

function measureInWindowAsync(node: number) {
  return new Promise<MeasuerInWindow>(resolve => {
    UIManager.measureInWindow(
      node,
      (x, y, width, height) => resolve({ x, y, width, height })
    );
  });
}

type MeasureLayout = {
  left: number;
  top: number;
  width: number;
  height: number;
};

function measureLayoutAsync(node: number, relativeToNativeNode: number) {
  return new Promise<MeasureLayout>((resolve, reject) => {
    UIManager.measureLayout(
      node,
      relativeToNativeNode,
      reject,
      (left, top, width, height) => resolve({ left, top, width, height })
    );
  });
}

export type KeyboardAvoidingScrollHOCProps = {
  keyboardAvoiding?: boolean;
  keyboardAvoidingPadding?: number;
};

export function withKeyboardAvoidingScroll<P extends ScrollViewProps>(
  Source: ComponentClass<P> & ScrollableComponentClass
) {
  const Result = forwardRef<Component<P>, P & KeyboardAvoidingScrollHOCProps>((props, ref) => {
    const { keyboardAvoiding = false, keyboardAvoidingPadding = 50 } = props;

    const sourceRef = useRef<Component<P> | null>();
    const [keyboardAvoidingHeight, setKeybaordAvoidingHeight] = useState(0);

    const keyboardShowHandler = async ({ endCoordinates, duration, easing }: KeyboardEvent) => {
      if (!keyboardAvoiding || !sourceRef.current) return;

      // @ts-ignore
      const focusedInputNode = TextInput.State.currentlyFocusedInput ?
        // @ts-ignore
        findNodeHandle(TextInput.State.currentlyFocusedInput()) :
        TextInput.State.currentlyFocusedField();
      if (!focusedInputNode) return;

      const containerNode = findNodeHandle(sourceRef.current);
      if (!containerNode) return;

      // @ts-ignore
      UIManager.viewIsDescendantOf(focusedInputNode, containerNode, async isAncestor => {
        if (!isAncestor || !sourceRef.current) return;

        const [containerMeasure, textInputMeasure] = await Promise.all([
          measureInWindowAsync(containerNode),
          measureLayoutAsync(focusedInputNode, containerNode)
        ]);

        const keyboardHeight = endCoordinates.height;
        const window = Dimensions.get('window');
        const containerBottom = window.height - (containerMeasure.y + containerMeasure.height);
        const avoiding = keyboardHeight - containerBottom;
        if (Platform.OS === 'ios') {
          LayoutAnimation.configureNext(LayoutAnimation.create(duration, easing, 'opacity'));
        }
        setKeybaordAvoidingHeight(avoiding);

        const visibleHeight = containerMeasure.height - avoiding;
        const textInputBottom = textInputMeasure.top + textInputMeasure.height;
        const scrollY = (textInputBottom + keyboardAvoidingPadding) - visibleHeight;

        setTimeout(() => {
          if (Source === ScrollView) {
            // @ts-ignore
            sourceRef.current.scrollTo({ y: scrollY });
          } else if (Source === SectionList) {
            // @ts-ignore
            sourceRef.current.getScrollResponder()?.scrollTo({ y: scrollY });
          } else if (Source === VirtualizedList || Source === FlatList) {
            // @ts-ignore
            sourceRef.current.scrollToOffset({ offset: scrollY });
          }
        }, 1);
      });
    };
    const keyboardHideHandler = ({ duration, easing }: KeyboardEvent) => {
      if (!keyboardAvoiding) return;

      if (Platform.OS === 'ios') {
        LayoutAnimation.configureNext(LayoutAnimation.create(duration, easing, 'opacity'));
      }
      setKeybaordAvoidingHeight(0);
    };

    useKeyboardEvent(Platform.select({
      ios: {
        onWillShow: keyboardShowHandler,
        onWillHide: keyboardHideHandler,
      },
      default: {
        onDidShow: keyboardShowHandler,
        onWillHide: keyboardHideHandler,
      },
    }));

    return (
      <Source
        {...props}
        automaticallyAdjustContentInsets={false}
        ref={current => {
          sourceRef.current = current;
          if (ref) {
            if (typeof ref === 'function') {
              ref(current);
            } else {
              ref.current = current;
            }
          }
        }}
        contentContainerStyle={[props.contentContainerStyle, {
          paddingBottom: keyboardAvoidingHeight,
        }]}
      />
    );
  });

  Result.displayName = `${Source.displayName}WithKeyboardAvoidingScroll`;
  return Result;
}
