import React, { ComponentType, FC } from 'react';

import { ThemeContext } from '../contexts';

export function withTheme<P>(Source: ComponentType<P>) {
  const SourceWithTheme: FC<P> = props => (
    <ThemeContext.Consumer>
      {theme => <Source theme={theme} {...props} />}
    </ThemeContext.Consumer>
  );

  SourceWithTheme.displayName = `${Source.displayName}WithTheme`;
  return SourceWithTheme;
}
