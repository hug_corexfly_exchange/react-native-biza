declare module '*.svg' {
  import Svg from 'react-native-svg'

  const content: Svg;
  export default content;
}

type UpperCaseHex = (
  '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' |
  'A' | 'B' | 'C' | 'D' | 'E' | 'F'
);
type LowerCaseHex = (
  '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' |
  'a' | 'b' | 'c' | 'd' | 'e' | 'f'
);
declare type UnsafetyHexColorType = (
  `#${UpperCaseHex}${string}${UpperCaseHex}` | 
  `#${LowerCaseHex}${string}${LowerCaseHex}`
);

declare type UnsafetyRGBAColorType = (
  `rgba(${number}, ${number}, ${number}, ${number})` |
  `RGBA(${number}, ${number}, ${number}, ${number})`
);

declare type ReservedColorNames = (
  'aliceblue' |
  'antiquewhite' |
  'aqua' |
  'aquamarine' |
  'azure' |
  'beige' |
  'bisque' |
  'black' |
  'blanchedalmond' |
  'blue' |
  'blueviolet' |
  'brown' |
  'burlywood' |
  'cadetblue' |
  'chartreuse' |
  'chocolate' |
  'coral' |
  'cornflowerblue' |
  'cornsilk' |
  'crimson' |
  'cyan' |
  'darkblue' |
  'darkcyan' |
  'darkgoldenrod' |
  'darkgray' |
  'darkgreen' |
  'darkgrey' |
  'darkkhaki' |
  'darkmagenta' |
  'darkolivegreen' |
  'darkorange' |
  'darkorchid' |
  'darkred' |
  'darksalmon' |
  'darkseagreen' |
  'darkslateblue' |
  'darkslategray' |
  'darkslategrey' |
  'darkturquoise' |
  'darkviolet' |
  'deeppink' |
  'deepskyblue' |
  'dimgray' |
  'dimgrey' |
  'dodgerblue' |
  'firebrick' |
  'floralwhite' |
  'forestgreen' |
  'fuchsia' |
  'gainsboro' |
  'ghostwhite' |
  'goldenrod' |
  'gold' |
  'gray' |
  'green' |
  'greenyellow' |
  'grey' |
  'honeydew' |
  'hotpink' |
  'indianred' |
  'indigo' |
  'ivory' |
  'khaki' |
  'lavenderblush' |
  'lavender' |
  'lawngreen' |
  'lemonchiffon' |
  'lightblue' |
  'lightcoral' |
  'lightcyan' |
  'lightgoldenrodyellow' |
  'lightgray' |
  'lightgreen' |
  'lightgrey' |
  'lightpink' |
  'lightsalmon' |
  'lightseagreen' |
  'lightskyblue' |
  'lightslategray' |
  'lightslategrey' |
  'lightsteelblue' |
  'lightyellow' |
  'lime' |
  'limegreen' |
  'linen' |
  'magenta' |
  'maroon' |
  'mediumaquamarine' |
  'mediumblue' |
  'mediumorchid' |
  'mediumpurple' |
  'mediumseagreen' |
  'mediumslateblue' |
  'mediumspringgreen' |
  'mediumturquoise' |
  'mediumvioletred' |
  'midnightblue' |
  'mintcream' |
  'mistyrose' |
  'moccasin' |
  'navajowhite' |
  'navy' |
  'oldlace' |
  'olive' |
  'olivedrab' |
  'orange' |
  'orangered' |
  'orchid' |
  'palegoldenrod' |
  'palegreen' |
  'paleturquoise' |
  'palevioletred' |
  'papayawhip' |
  'peachpuff' |
  'peru' |
  'pink' |
  'plum' |
  'powderblue' |
  'purple' |
  'rebeccapurple' |
  'red' |
  'rosybrown' |
  'royalblue' |
  'saddlebrown' |
  'salmon' |
  'sandybrown' |
  'seagreen' |
  'seashell' |
  'sienna' |
  'silver' |
  'skyblue' |
  'slateblue' |
  'slategray' |
  'slategrey' |
  'snow' |
  'springgreen' |
  'steelblue' |
  'tan' |
  'teal' |
  'thistle' |
  'tomato' |
  'transparent' |
  'turquoise' |
  'violet' |
  'wheat' |
  'white' |
  'whitesmoke' |
  'yellow' |
  'yellowgreen'
);
